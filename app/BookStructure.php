<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookStructure extends Model
{
    protected $table = 'book_structure';
    public $timestamps = false;
}
