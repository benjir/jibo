<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $table = 'info_candidate';

    protected $fillable = [
                            'name',
                            'mobile',
                            'address',
                            'gender',
                            'nationality',
                            'education_level',
                            'language',
                            'religion',
                            'level_id',
                            'category_id',
                            'height',
                            'birthdate',
                            'photo',
                            'status',
                          ];
    public $timestamps = false;


    public function service()
    {
      return $this->belongsTo('App\Service', 'category_id');
    }
    public function office()
    {
      return $this->belongsTo('App\Office', 'mobile');
    }
    public function level()
    {
      return $this->belongsTo('App\Level', 'level_id');
    }
    public function orders()
    {
      return $this->hasMany(\App\Order::class);
    }
    public function review()
    {
      return $this->hasManyThrough(\App\Review::class, \App\Order::class);
    }
}
