<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $id = auth()->user()->id;
        $this->guard()->logout();

        \App\LogRecord::create([
          "admin_id" => $id,
          "login_time" => null,
          "logout_time" => \Carbon\Carbon::now(),
          "online_status" => 1,
        ]);

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }

    protected function authenticated(Request $request, $user)
    {
      \App\LogRecord::create([
        "admin_id" => $user->id,
        "login_time" => \Carbon\Carbon::now(),
        "logout_time" => null,
        "online_status" => 1,
      ]);
    }
}
