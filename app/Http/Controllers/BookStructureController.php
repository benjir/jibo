<?php

namespace App\Http\Controllers;

use App\BookStructure;
use Illuminate\Http\Request;

class BookStructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookStructure  $bookStructure
     * @return \Illuminate\Http\Response
     */
    public function show(BookStructure $bookStructure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookStructure  $bookStructure
     * @return \Illuminate\Http\Response
     */
    public function edit(BookStructure $bookStructure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookStructure  $bookStructure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookStructure $bookStructure)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookStructure  $bookStructure
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookStructure $bookStructure)
    {
        //
    }
}
