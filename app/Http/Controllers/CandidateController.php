<?php

namespace App\Http\Controllers;

use App\Candidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CandidateController extends Controller
{
  public function __construct(Candidate $candidate)
  {
    $this->candidate = $candidate;
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('admin.candidates.index')->with('candidates', $this->candidate->paginate(12));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.candidates.create')
              ->with('levels', \App\Level::all())
              ->with('services', \App\Service::all());
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // dd($request->all());
    $this->validator($request->all())->validate();

    if ($request->hasFile('photo')) {
      $photo = $request->file('photo');
      $photo_name = str_slug($request->name) . '.' . $photo->getClientOriginalExtension();
      $folder = public_path('JIBU/assets/candidates/');

      if (file_exists($folder.$photo_name)) unlink($folder.$photo_name);

      $photo->move($folder, $photo_name);
    }

    $input = $request->all();
    $input['mobile'] = 1; // NOTE: it's just for now, I have to discuss
    $input['photo'] = $photo_name;

    $candidate = $this->candidate->create($input);

    return ($candidate) ?
              redirect()->back()->with('success', 'successfully created')
              : redirect()->back()->with('error', 'something wrong');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Candidate  $candidate
   * @return \Illuminate\Http\Response
   */
  public function show(Candidate $candidate)
  {
      $orders = $candidate->orders()->paginate(10);
      return view('admin.candidates.show', compact('candidate', 'orders'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Candidate  $candidate
   * @return \Illuminate\Http\Response
   */
  public function edit(Candidate $candidate)
  {
      return view('admin.candidates.edit', compact('candidate'))
              ->with('levels', \App\Level::all())
              ->with('services', \App\Service::all());
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Candidate  $candidate
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Candidate $candidate)
  {
    $this->validator($request->all())->validate();

    if ($request->hasFile('photo')) {
      $photo = $request->file('photo');
      $photo_name = str_slug($request->name) . '.' . $photo->getClientOriginalExtension();
      $folder = public_path('JIBU/assets/candidates/');

      if (file_exists($folder.$candidate->photo ?? 'abc.jpg')) unlink($folder.$candidate->photo ?? 'abc.jpg');
      if (file_exists($folder.$photo_name)) unlink($folder.$photo_name);

      $photo->move($folder, $photo_name);
    }
    $input = $request->all();
    $input['photo'] = $photo_name ?? $candidate->photo;
    $input['birthdate'] = \Carbon\Carbon::parse($request->birthdate);

    return $candidate->update($input) ?
                          redirect()->back()->with('success', 'successfully updated')
                          : redirect()->back()->with('error', 'something wrong');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Candidate  $candidate
   * @return \Illuminate\Http\Response
   */
  public function destroy(Candidate $candidate)
  {
    if ($candidate->photo) {
      $folder =  public_path('JIBU/assets/Candidates/');
      if (file_exists($folder.$candidate->photo)) unlink($folder.$candidate->photo);
    }

    try {

      return ($candidate->delete()) ?
                redirect()->route('candidates.index')->with('success', 'successfully Deleted')
                : redirect()->route('candidates.index')->with('error', 'something wrong');

    } catch (\Illuminate\Database\QueryException $e) {
        return redirect()->route('candidates.index')->with('error', 'something wrong, contact with support');
    }
  }

  public function hide(Candidate $candidate)
  {
    try {
      $candidate->status = $candidate->status ? 0 : 1;
      $candidate->save();
      return redirect()->back();
    } catch (\Exception $e) {
      return redirect()->route('candidates.index')->with('error', 'something wrong, contact with support');
    }

  }

  protected function validator(array $data)
  {
    $rules = [
      'name' => 'required',
      'address' => 'required',
      'gender' => 'required',
      'nationality' => 'required',
      'education_level' => 'required',
      'language' => 'required',
      'religion' => 'required',
      'level_id' => 'required|numeric',
      'category_id' => 'required|numeric',
      'height' => 'required',
      'birthdate' => 'required',
      'photo' => 'nullable|mimetypes:image/png,image/jpeg,image/jpg',
    ];
    return Validator::make($data, $rules,[
      'category_id.numeric' => 'Please Select Category',
      'level_id.numeric' => 'Please Select Level',
      'photo.mimetypes' => 'Photo Must be .png/.jpg/.jpeg type',
    ]);
  }
}
