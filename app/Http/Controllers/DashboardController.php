<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sessions\Online;
use App\User;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $all_staff = User::with('online')->get();

      $all_staff->map(function ($item, $key){
        if (!is_null($item->online)) {
          $item->online->payload = collect(
            unserialize(base64_decode($item->online->payload))
          );
          $item->online->last_activity = \Carbon\Carbon::createFromTimestamp($item->online->last_activity)->toDateTimeString();
        }
      });

      return view('dashboard', compact('all_staff'));
    }

    public function userLog(User $user)
    {
      if (!is_null($user->online)) {
        $user->online->last_activity = \Carbon\Carbon::createFromTimestamp($user->online->last_activity)->toDateTimeString();
      }

      return view('admin.user-log', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
