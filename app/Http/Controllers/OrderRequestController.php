<?php

namespace App\Http\Controllers;

use App\OrderRequest;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class OrderRequestController extends Controller
{
    public function __construct(OrderRequest $order_request)
    {
        $this->order_request = $order_request;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $candidates = \App\Candidate::all()->pluck('name');
        $order_requests = $this->order_request->orderBy('id', 'desc')->paginate(15);
        return view('admin.orders.request', compact('order_requests', 'candidates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assign(Request $request)
    {

        DB::transaction(function () use ($request)
        {
            $candidate = \App\Candidate::where('name',$request->candidate)->first();
            $order_request = $this->order_request->where('id', $request->request_id)->first()->toArray();

            if (!$candidate || !$order_request) {
                return back();
            }

            $order = new \App\Order();
            $order->fill($order_request);
            $order->order_number = (string)$order_request['order_number'];
            $order->candidate_id = $candidate->id;
            $order->book_id = $order_request['book_structure_id'];
            $order->from_request = 1;
            $order->save();

            $this->order_request->find($order_request['id'])->update(['status' => 2]);

            // "pick_time" => null
            //
            // 'confirm_time',
            // 'finish_time',
        });
        return redirect()->to(route('orders.request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $order_request = $this->order_request->find($request->id);

        $candidate = \App\Candidate::where('category_id', $order_request->category_id)
                                    ->where('name', 'like', '%'. $request->name . '%')
                                    ->get()->pluck('name');
        return response()->json($candidate);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderRequest  $orderRequest
     * @return \Illuminate\Http\Response
     */
    public function show(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderRequest  $orderRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderRequest  $orderRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderRequest  $orderRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->order_request->where('id', $id)->delete()) {
            return redirect()->back();
        }
        return redirect()->back();
    }
}
