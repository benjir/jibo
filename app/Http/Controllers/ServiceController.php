<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function __construct(Service $service)
    {
      $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.services.index')->with('services', $this->service->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validator($request->all())->validate();

      if ($request->hasFile('photo')) {
        $photo = $request->file('photo');
        $photo_name = 'ic_'. str_slug($request->name) . '.' . $photo->getClientOriginalExtension();
        $folder = public_path('JIBU/assets/categories/');

        if (file_exists($folder.$photo_name)) unlink($folder.$photo_name);

        $photo->move($folder, $photo_name);
      }
      if ($request->hasFile('photo_hi')) {
        $photo_hi = $request->file('photo_hi');
        $photo_hi_name = 'hi_'. str_slug($request->name) . '.' . $photo_hi->getClientOriginalExtension();
        $folder = public_path('JIBU/assets/categories/');

        if (file_exists($folder.$photo_hi_name)) unlink($folder.$photo_hi_name);

        $photo_hi->move($folder, $photo_hi_name);
      }

      $service = $this->service->create([
        'name' => $request->name,
        'photo' => $photo_name ?? 'no_icon.png',
        'photo_hi' => $photo_hi_name ?? 'no_image.jpg'
      ]);

      return ($service) ?
                redirect()->back()->with('success', 'successfully created')
                : redirect()->back()->with('error', 'something wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
      $candidates = $service->candidates()->paginate(10);

      return view('admin.services.show', compact('service', 'candidates'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
      $this->validator($request->all())->validate();

      if ($request->hasFile('photo')) {
        $photo = $request->file('photo');
        $photo_name = 'ic_'. str_slug($request->name) . '.' . $photo->getClientOriginalExtension();
        $folder = public_path('JIBU/assets/categories/');

        if (file_exists($folder.$service->photo ?? 'abc.jpg')) unlink($folder.$service->photo ?? 'abc.jpg');
        if (file_exists($folder.$photo_name)) unlink($folder.$photo_name);

        $photo->move($folder, $photo_name);
        $service->photo = $photo_name;
      }
      if ($request->hasFile('photo_hi')) {
        $photo_hi = $request->file('photo_hi');
        $photo_hi_name = 'hi_'. str_slug($request->name) . '.' . $photo_hi->getClientOriginalExtension();
        $folder = public_path('JIBU/assets/categories/');

        if (file_exists($folder.$service->photo_hi ?? 'abc.jpg')) unlink($folder.$service->photo_hi ?? 'abc.jpg');
        if (file_exists($folder.$photo_hi_name)) unlink($folder.$photo_hi_name);

        $photo_hi->move($folder, $photo_hi_name);
        $service->photo_hi = $photo_hi_name;
      }

      $service->name = $request->name;

      return $service->save() ?
                            redirect()->back()->with('success', 'successfully updated')
                            : redirect()->back()->with('error', 'something wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
      if ($category->image) {
        $folder =  public_path('images/');
        if (file_exists($folder.$category->image)) unlink($folder.$category->image);
      }

      $category_id = Category::where('id', '!=', $category->id)->first()->id;

      $category->allItems->each(function ($item) use ($category_id){
          $item->update(['category_id' => $category_id ]);
      });

      return ($category->delete()) ?
                redirect()->route('admin.categories.index')->with('success', 'successfully Updated')
                : redirect()->route('admin.categories.index')->with('error', 'something wrong');
    }
    protected function validator(array $data)
    {
      $rules = [
        'name' => 'required',
        'photo' => 'nullable|mimetypes:image/png',
        'photo_hi' => 'nullable|mimetypes:image/jpeg,image/jpg',
      ];
      return Validator::make($data, $rules,[
        'photo.mimetypes' => 'Icon Must be .png type',
        'photo_hi.mimetypes' => 'Big Photo Must be .jpg or .jpeg type'
      ]);
    }
}
