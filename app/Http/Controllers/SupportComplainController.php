<?php

namespace App\Http\Controllers;

use App\supportComplain;
use Illuminate\Http\Request;

class SupportComplainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\supportComplain  $supportComplain
     * @return \Illuminate\Http\Response
     */
    public function show(supportComplain $supportComplain)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\supportComplain  $supportComplain
     * @return \Illuminate\Http\Response
     */
    public function edit(supportComplain $supportComplain)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\supportComplain  $supportComplain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, supportComplain $supportComplain)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\supportComplain  $supportComplain
     * @return \Illuminate\Http\Response
     */
    public function destroy(supportComplain $supportComplain)
    {
        //
    }
}
