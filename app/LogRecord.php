<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogRecord extends Model
{
    protected $table = 'login_record_admin';
    protected $fillable = ["admin_id",	"login_time",	"logout_time",	"online_status"];
    public $timestamps = false;

}
