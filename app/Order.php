<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    public $timestamps = false;

    protected $fillable = [
        'order_number',
        'customer_id',
        'candidate_id',
        'book_id',
        'duration',
        'amount',
        'phone',
        'location',
        'start_time',
        'end_time',
        'status',
        'from_request',
        'order_time',
        'confirm_time',
        'finish_time',
    ];

    public function book()
    {
        return $this->belongsTo(BookStructure::class, 'book_id');
    }
    public function candidate()
    {
        return $this->belongsTo(Candidate::class, 'candidate_id');
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
