<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRequest extends Model
{
    protected $table = 'request_order';

    public $timestamps = false;

    protected $fillable = ['status'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
    public function service()
    {
        return $this->belongsTo(Service::class, 'category_id');
    }
    public function book()
    {
        return $this->belongsTo(BookStructure::class, 'book_structure_id');
    }
}
