<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'info_category';

    protected $fillable = [ 'name','photo','photo_hi'];

    public $timestamps = false;


    public function candidates()
    {
      return $this->hasMany('App\Candidate', 'category_id');
    }
}
