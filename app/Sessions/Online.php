<?php

namespace App\Sessions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Online extends Model
{
    protected $table = 'sessions';
    protected $fillable = ['user_id', 'ip_address', 'user_agent', 'payload', 'last_activity'];

    // protected $appends = ['last_active'];

    // public static function all()
    // {
    //   $sessions = DB::table('sessions')->get();
    //
    //   $all = collect(null);
    //
    //   foreach($sessions as $key => $session) {
    //     $all->push([
    //       'last_activity' => \Carbon\Carbon::createFromTimestamp($session->last_activity)->toDateTimeString(),
    //       'user_id' => $session->user_id,
    //       'ip_address' => $session->ip_address,
    //     ]);
    //   }
    //   return $all;
    // }
    //
    // public function getUser()
    // {
    //   $all = self::all();
    //
    // }
    // public function getLastActivityAttribute () {
    //   $this->last_active = \Carbon\Carbon::createFromTimestamp($this->last_activity)->toDateTimeString();
    // }
}
