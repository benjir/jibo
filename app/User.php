<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ADMIN = 1;
    const STAFF = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'role',
    ];

    protected $table = 'info_admin';

    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function online()
    {
      return $this->hasOne('App\Sessions\Online');
    }

    public function log()
    {
      return $this->hasMany('App\LogRecord', 'admin_id')->orderBy('id', 'desc');
    }

    public function login()
    {
      return $this->log()->whereNotNull('login_time');
    }
    public function logout()
    {
      return $this->log()->whereNotNull('logout_time');
    }

    public function role()
    {
      switch ($this->role) {
        case self::ADMIN:
          return 'admin';
          break;
        case self::STAFF:
          return 'staff';
          break;

        default:
          return 'none';
          break;
      }
    }
}
