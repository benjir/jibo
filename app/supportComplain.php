<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class supportComplain extends Model
{
    protected $table = 'support_complains';

    public $timestamps = false;

    public function complain()
    {
        return $this->where('type', 1);
    }
}
