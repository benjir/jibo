-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 09, 2019 at 09:03 AM
-- Server version: 10.1.37-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `infinehm_jibu`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_structure`
--

CREATE TABLE `book_structure` (
  `id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  `cost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_structure`
--

INSERT INTO `book_structure` (`id`, `name`, `cost`) VALUES
(1, 'hour', 100),
(2, 'day', 300),
(3, 'week', 600),
(4, 'month', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `customer_id`, `candidate_id`) VALUES
(4, 1, 6),
(8, 1, 9),
(13, 1, 10),
(6, 3, 7),
(17, 4, 6),
(14, 4, 7),
(21, 4, 8),
(19, 4, 9),
(18, 4, 10),
(20, 4, 11),
(34, 16, 12);

-- --------------------------------------------------------

--
-- Table structure for table `history_notification`
--

CREATE TABLE `history_notification` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender` int(11) NOT NULL,
  `receiver` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `send_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `history_notification`
--

INSERT INTO `history_notification` (`id`, `title`, `body`, `image`, `action`, `sender`, `receiver`, `status`, `send_time`) VALUES
(1, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 1, 'all', 1, '2018-11-19 13:17:39'),
(2, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 1, 'all', 1, '2018-11-19 13:18:00'),
(3, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', 'images.png', NULL, 1, 'all', 1, '2018-11-19 13:19:37'),
(4, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 2, 'all', 1, '2018-11-19 13:20:23'),
(5, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 1, 'all', 1, '2018-11-19 13:18:30'),
(6, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 1, '12', 1, '2018-11-19 13:21:13'),
(7, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 1, 'all', 1, '2018-11-19 13:18:30'),
(8, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', 'night_train.jpg', NULL, 1, 'all', 1, '2018-11-19 13:19:52'),
(9, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 3, 'all', 1, '2018-11-19 13:20:36'),
(10, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 4, '11', 1, '2018-11-19 13:20:56'),
(11, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 1, 'all', 1, '2018-11-19 13:18:30'),
(12, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', 'night_train.jpg', NULL, 1, 'all', 1, '2018-11-19 13:19:57'),
(13, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 1, 'all', 1, '2018-11-19 13:18:30'),
(14, 'This is test notification header/title', 'This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body This is test notification body ', NULL, NULL, 1, 'all', 1, '2018-11-19 13:18:30');

-- --------------------------------------------------------

--
-- Table structure for table `info_admin`
--

CREATE TABLE `info_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `joining_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remember_token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info_admin`
--

INSERT INTO `info_admin` (`id`, `name`, `mobile`, `email`, `password`, `role`, `status`, `joining_date`, `remember_token`) VALUES
(1, 'Admin', '+8801683505006', 'admin@jibu.com', '$2y$10$YXClXNLpExJ7zXLzsqBmmu9VhsIgtHDmri72K6dh1IDJgMqSUs4ce', 1, 1, '2018-09-17 03:00:09', 'gwEPvIVyhk5tiXXORCJY0bh138OMPHD0EHy8rDN7VlCsmiC0zUxYGe55Q4g1'),
(2, 'Admin', '+8801683505007', 'admin1@jibu.com', '$2y$10$KF7zLDlnr2fVdwrC1NtST.rLWhshui65ZHJGbdgc.GaKcUT7ONo72', 1, 1, '2018-09-17 03:18:01', NULL),
(3, 'admin', '+93746349887485', 'admin@gmail.com', '$2y$10$R4i2mPabNK.3MiURKmhXUuFJj8S.t7X4b.jCBWfPg1i46ri8bbpzS', 1, 1, '2018-10-01 05:18:41', 'EnfdsNmgFU5AVlgHytByemrFBwxMk1jFgDPmjeoVKzYgzjU2Kv46LujeRC04'),
(4, 'staff', '+93746349887485', 'staff@gmail.com', '$2y$10$FFkKussGEWpeLJqsG3Qji..1fToZ657OnkGh4aW8h1AP8K1xPmV3S', 2, 0, '2018-10-02 01:26:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `info_candidate`
--

CREATE TABLE `info_candidate` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `nationality` varchar(30) NOT NULL,
  `education_level` varchar(30) NOT NULL,
  `language` varchar(50) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `level_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `height` varchar(10) NOT NULL,
  `birthdate` date NOT NULL,
  `photo` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `experience` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info_candidate`
--

INSERT INTO `info_candidate` (`id`, `name`, `mobile`, `address`, `gender`, `nationality`, `education_level`, `language`, `religion`, `level_id`, `category_id`, `height`, `birthdate`, `photo`, `status`, `experience`) VALUES
(6, 'Abul', 1, 'Dhanmondi, Dhaka, Bangladesh', 'male', 'Bangladeshi', 'SSC', 'Bangla', 'Islam', 1, 2, '5\'4\"', '1990-08-02', 'abul.jpg', 0, NULL),
(7, 'Aslam Haque', 1, 'Mirpur, Dhaka, Bangladesh', 'male', 'Bangladeshi', 'HSC', 'Bangla', 'Islam', 3, 4, '5\'8\"', '1988-09-02', 'aslam.jpg', 1, NULL),
(8, 'Chan Miah', 1, 'Banani, Dhaka, Bangladesh', 'male', 'Bangladeshi', 'JSC', 'Bangla', 'Islam', 1, 7, '5\'5\"', '1985-09-02', 'chan.jpg', 1, NULL),
(9, 'Moyna', 1, 'Banani, Dhaka, Bangladesh', 'female', 'Bangladeshi', 'JSC', 'Bangla', 'Islam', 2, 4, '5\'2\"', '1987-09-02', 'moyna.jpg', 1, NULL),
(10, 'Rubel Molla', 1, 'Badda, Dhaka, Bangladesh', 'male', 'Bangladeshi', 'HSC', 'Bangla', 'Islam', 3, 4, '6\'1\"', '1992-09-02', 'rubel.jpg', 1, NULL),
(11, 'Jui Titli', 1, 'Shantibagh, Dhaka, Bangladesh', 'female', 'Bangladeshi', 'BSc', 'English', 'Islam', 3, 5, '5\'5\"', '1995-09-30', 'jui.jpg', 1, NULL),
(12, 'Sumon Ahmed', 1, '72 Shyamoli, Dhaka 1218', 'male', 'Bangladeshi', 'Graduated', 'English', 'Islam', 2, 1, '6\'2', '1990-05-04', 'sumon-ahmed.jpg', 1, NULL),
(13, 'Mahi Chowdhury', 1, '55 Kafrul, Dhaka 1226', 'female', 'Bangladeshi', 'Graduated', 'Bangla', 'Islam', 3, 8, '5\'5', '1995-05-05', 'mahi-chowdhury.jpg', 1, NULL),
(14, 'Rajib Al Hasan', 1, 'Lake circus road Dhaka Bangladesh', 'male', 'Bangladeshi', 'Graduate', 'English', 'Islam', 3, 9, '5\'8', '1995-06-05', 'rajib-al-hasan.png', 1, NULL),
(15, 'Shreya Ghosh', 1, 'Mohakhali Dhaka, Bangladesh', 'female', 'Bangladeshi', 'Graduate', 'English', 'Hindu', 2, 4, '5\'8', '1992-03-05', 'shreya-ghosh.jpg', 1, NULL),
(16, 'Mrittika Rahman', 1, 'Dhanmondhi, Dhaka - 1205', 'female', 'Bangladeshi', 'Graduated', 'English', 'Islam', 3, 4, '5\'5', '1993-12-12', 'mrittika-rahman.jpg', 1, NULL),
(17, 'Monika Belucci', 1, 'Savar, Dhaka Bangladesh', 'female', 'Bangladeshi', 'Graduate', 'English', 'Islam', 1, 2, '5\'5', '1991-02-02', 'monika-belucci.jpg', 1, NULL),
(18, 'Shilpa Shethi', 1, 'Manik Nagar, Rajshahi, Bangladesh', 'female', 'Bangladeshi', 'Graduate', 'English', 'Hindu', 2, 3, '5\'5', '1996-06-06', 'shilpa-shethi.jpg', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `info_category`
--

CREATE TABLE `info_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `photo_hi` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info_category`
--

INSERT INTO `info_category` (`id`, `name`, `photo`, `photo_hi`, `status`) VALUES
(1, 'Home Assistants', 'ic_home-assistants.png', 'hi_home_assistants.jpg', 1),
(2, 'Nannies', 'ic_nannies.png', 'hi_nannies.jpg', 1),
(3, 'Carers', 'ic_carers.png', 'hi_carers.jpg', 1),
(4, 'Cooks / Chefs', 'ic_cooks-chefs.png', 'hi_chefs.jpg', 1),
(5, 'Students', 'ic_students.png', 'hi_students.jpg', 1),
(6, 'Cleaners', 'ic_cleaners.png', 'hi_cleaners.jpg', 1),
(7, 'Drivers', 'ic_drivers.png', 'hi_drivers.jpg', 1),
(8, 'Artisans', 'ic_artisans.png', 'hi_artisans.jpg', 1),
(9, 'Tutor', 'ic_tutor.png', 'hi_tutor.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `info_customer`
--

CREATE TABLE `info_customer` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `joining_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fcm_token` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info_customer`
--

INSERT INTO `info_customer` (`id`, `name`, `email`, `mobile`, `password`, `photo`, `status`, `joining_date`, `fcm_token`) VALUES
(1, 'Md Arif', 'arif@email.com', '01720202020', '$2y$10$Y5M8NQfUWQmpBN7V8PnR9.AJFg5tm/33Q01SZEZiv22uh.HkTt2xy', '1538083490_350543_495150_380810.jpg', 1, '2018-09-17 02:53:48', NULL),
(2, 'Md Arif', 'arif1@email.com', '01720203041', '$2y$10$5Q.Xgl85gTb8.LqpLCzTnuv4yqM/s6m3WedYJHxvl2yttqb594l/K', NULL, 0, '2018-09-17 02:55:05', NULL),
(3, 'Md Arif', 'arif2@email.com', '01720203042', '$2y$10$JcpdCUF.l0KFOCeWGboG2uwIiQ1KBjvjblCUsxuSO4aICFEZGIMPG', NULL, 0, '2018-09-17 03:16:43', NULL),
(4, 'Md Arif', 'arif991846@gmail.com', '01683505006', '$2y$10$.IBb/ghL1Qfe1khKRy5zUO1z0/hiNmDa0TrdTJ4MMbi0Ea5nSREey', '1538244764_568649_910900_796011.jpg', 1, '2018-09-20 15:02:55', '1212'),
(5, 'Anuradha', 'anu@email.com', '01683505000', '$2y$10$3ouWXGKJriVZxZ2xZiXm9.Prn2/VVO4noYYBWDveTP9opq0WrOC8W', '1538075868_772720_345269_820346.jpg', 1, '2018-09-20 15:08:23', NULL),
(6, 'Rabbi Rahman', 'rabbi@email.com', '01683505009', '$2y$10$QvPWk5fYN.iMYoIRb/RsduYZLc4d.RC9GtZ3agHqF556yKigKQRiq', NULL, 0, '2018-09-24 19:18:26', NULL),
(7, 'Motin Mia', 'motinrabbi@email.com', '01683505001', '$2y$10$7VVVSwCoTLkYmK7UF8CjT.Yg1fVsbk2a9S9BEVZMuuC81OquUW0Jy', '1538076701_356420_601443_519842.jpg', 1, '2018-09-27 11:52:07', NULL),
(8, 'Anil kumble', 'anil@email.com', '01683505002', '$2y$10$Ki/pVzDjd0m3szAbLSSgvuec4aZaundFV3LIEU63sNm.ib7Yhf.Ae', '1538077868_217526_213488_153091.jpg', 1, '2018-09-27 11:55:43', NULL),
(9, 'Rony Ahmed', 'rony@email.com', '01683505003', '$2y$10$t4VEbc0bkXbnkBD6UxG85uzKPHOgDR6YP.nW3oG5MmX7yb3mgYc3y', NULL, 0, '2018-09-27 11:57:31', NULL),
(12, 'Mr Test', 'test@gmail.com', '1234', '$2y$10$.ccpjxsGhvAvgItUWb4nwuxCQGbvmIllMcJMs8kNzmQsf3p3bcUZ6', '1549392455_486275_995657_426813.jpg', 1, '2018-11-19 13:51:44', 'dIsSfTRWG5M:APA91bE4uyY7w1EFHuo450W2GlU5BPD8LLtl3jVE75gfqmhBhVfUH-9NOJeR_C01rHXdSEqKdy4ndn81E4hj7f6DlE4bLZkAMOmfTUefU_6Zad6GiFCJzI-YZn91CSfUhOihTTn0ZWjZ'),
(13, 'Mr Test1', 'test1@gmail.com', '12345', '$2y$10$sZM0ZYDUscgn95uNXdqfH.pVWghTRoEVeYSRgMD/OpvpWVdsEEDEO', NULL, 0, '2018-11-19 13:54:25', 'ddddddddd'),
(14, 'Mr Test2', 'test2@gmail.com', '123456', '$2y$10$ETYR.G1oboYZw3mIlBChpOw2XxPXcVSMoG.Tt34cR.v43Bqq/OkUO', NULL, 1, '2018-11-19 13:56:26', 'ddddddddd'),
(15, 'What', 'what@gmail.com', '0165353532', '$2y$10$wzKhTclp1EJqMlOGquTPOOLZzfAqwAAPfhfEPnG.jks3AcCKEWyma', '1548601675_703711_663761_894085.jpg', 1, '2019-01-27 15:06:24', 'fFmbQOKVzUg:APA91bGKmuOr7FJ5tHg-DVTHblFf5aack-gd7vTk-SGaQr4_ggW198ss1xWIFY4dUKw_et1x4fjdrQKrRZ6ql_LpXuhIuc6bYZWxJuD6D-AyyP8EnseIVqFrMMFYXFFJAAgmhsMafSQ0'),
(16, 'gifty', 'kwamegyesi88@gmail.com', '0207962747', '$2y$10$onKe.Y5Df8Ejv1YEQqqGguOzEBLH4ZwMbVBTcWK/7goGMiy/D1dRi', NULL, 1, '2019-02-06 09:43:17', 'd89EBda0YMU:APA91bH1v7zULWnjJZdjCfZIHGs62WNJNl_zQESwpE0g9y6ourl7AKN0muJSzP6f9YlgT9XRHtp8a2wE3rMnGB5V7BYu5KelCEtJcfurTPkOzaf6dI5yJgoERw051ufELmjPneRjEZvh');

-- --------------------------------------------------------

--
-- Table structure for table `login_record_admin`
--

CREATE TABLE `login_record_admin` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `login_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logout_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `online_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_record_admin`
--

INSERT INTO `login_record_admin` (`id`, `admin_id`, `login_time`, `logout_time`, `online_status`) VALUES
(17, 3, NULL, '2018-10-04 07:21:21', 1),
(18, 3, '2018-10-04 07:21:32', NULL, 1),
(19, 3, NULL, '2018-10-04 07:21:59', 1),
(20, 3, '2018-10-04 07:22:08', NULL, 1),
(21, 3, NULL, '2018-10-04 07:22:58', 1),
(22, 3, '2018-10-04 07:23:21', NULL, 1),
(23, 3, NULL, '2018-10-04 07:46:22', 1),
(24, 3, '2018-10-04 07:46:33', NULL, 1),
(25, 3, '2018-10-04 17:16:13', NULL, 1),
(26, 3, '2018-10-05 09:28:22', NULL, 1),
(27, 3, '2018-10-05 12:44:11', NULL, 1),
(28, 3, NULL, '2018-10-05 14:19:26', 1),
(29, 3, '2018-10-05 14:34:34', NULL, 1),
(30, 3, '2018-10-09 07:47:39', NULL, 1),
(31, 3, '2018-10-10 04:05:00', NULL, 1),
(32, 3, '2018-10-10 09:20:32', NULL, 1),
(33, 3, '2018-10-27 20:22:18', NULL, 1),
(34, 3, NULL, '2018-10-27 20:24:25', 1),
(35, 3, '2018-10-27 20:24:33', NULL, 1),
(36, 3, '2018-10-29 17:01:37', NULL, 1),
(37, 3, '2018-11-03 14:30:08', NULL, 1),
(38, 3, '2018-11-04 15:34:47', NULL, 1),
(39, 3, '2018-11-05 05:07:54', NULL, 1),
(40, 3, '2018-11-05 16:51:33', NULL, 1),
(41, 3, '2018-11-08 08:02:04', NULL, 1),
(42, 3, '2018-11-09 18:42:37', NULL, 1),
(43, 3, '2018-11-11 05:00:33', NULL, 1),
(44, 3, '2018-11-11 18:34:13', NULL, 1),
(45, 3, '2018-11-12 20:40:58', NULL, 1),
(46, 3, '2018-11-13 18:13:13', NULL, 1),
(47, 1, '2019-02-06 03:48:35', NULL, 1),
(48, 1, NULL, '2019-02-06 05:09:26', 1),
(49, 1, '2019-02-06 05:09:35', NULL, 1),
(50, 1, NULL, '2019-02-06 05:17:10', 1),
(51, 1, '2019-02-06 05:18:22', NULL, 1),
(52, 1, NULL, '2019-02-06 05:18:38', 1),
(53, 1, '2019-02-06 05:24:55', NULL, 1),
(54, 3, '2019-02-06 05:38:55', NULL, 1),
(55, 1, '2019-02-07 03:34:35', NULL, 1),
(56, 1, '2019-02-07 03:34:58', NULL, 1),
(57, 1, '2019-02-08 15:52:59', NULL, 1),
(58, 1, '2019-02-09 00:29:37', NULL, 1),
(59, 1, '2019-02-09 01:52:22', NULL, 1),
(60, 1, NULL, '2019-02-09 02:22:19', 1),
(61, 1, '2019-02-09 02:27:55', NULL, 1),
(62, 1, NULL, '2019-02-09 02:29:57', 1),
(63, 1, '2019-02-09 21:12:50', NULL, 1),
(64, 1, '2019-02-10 00:01:22', NULL, 1),
(65, 1, NULL, '2019-02-10 00:47:18', 1),
(66, 1, '2019-02-10 00:48:17', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2018_10_02_004214_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `office_info`
--

CREATE TABLE `office_info` (
  `id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `office_info`
--

INSERT INTO `office_info` (`id`, `phone`, `email`, `address`, `status`, `latitude`, `longitude`) VALUES
(1, '+8801683505006', 'office@jibu.com', 'Dhaka', 1, '23.7568576', '90.379553');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `order_number` varchar(17) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `from_request` tinyint(4) DEFAULT NULL,
  `order_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `confirm_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `order_number`, `customer_id`, `candidate_id`, `book_id`, `duration`, `amount`, `phone`, `location`, `start_time`, `end_time`, `status`, `from_request`, `order_time`, `confirm_time`, `finish_time`) VALUES
(1, 'EDGA5678912345678', 12, 6, 1, 1, 100, '+8801683505006', 'Kalabagan, Dhaka-1205', NULL, NULL, 3, 0, '2018-11-26 06:53:47', '2018-09-24 01:00:00', '2018-09-24 17:00:00'),
(2, '4RDA5678912355678', 12, 6, 1, 1, 100, '+8801683505006', 'Kalabagan, Dhaka-1205', '2018-10-03 14:00:00', '2018-10-03 15:00:00', 3, 0, '2018-11-26 06:54:04', '2018-09-24 01:00:00', '2018-09-24 17:00:00'),
(3, '12346579812345762', 4, 10, 2, 1, 300, '+8801683505006', 'Kalabagan, Dhaka-1205', '2018-10-04 00:00:00', '2018-10-05 00:00:00', 3, 0, '2018-11-19 13:40:21', '2018-09-20 10:30:00', '2018-09-27 00:00:00'),
(14, '7A851538819702071', 2, 11, 4, 10, 10000, '+8801683505006', 'Kalabagan, Dhaka-1205', '2018-10-01 10:00:00', '2018-10-05 10:00:00', 3, 0, '2018-10-22 16:10:19', NULL, NULL),
(28, '49281538835822149', 12, 11, 4, 2, 2000, '+8801683505006', 'Sukrabad, Dhaka', '2019-02-21 00:00:00', '2019-04-21 00:00:00', 1, 0, '2019-02-05 18:22:18', NULL, NULL),
(29, 'D4JK1538835822149', 2, 11, 4, 2, 2000, '+8801683505006', 'Sukrabad, Dhaka', '2019-02-19 00:00:00', '2019-04-19 00:00:00', 2, 0, '2019-02-05 18:22:47', '2018-10-23 09:07:50', NULL),
(30, 'F2E51540549913702', 2, 11, 4, 10, 10000, '+8801683505006', 'Kalabagan, Dhaka-1205', '2019-02-28 10:00:00', '2019-12-28 10:00:00', 1, 0, '2019-02-05 18:23:28', NULL, NULL),
(31, 'BA6B1540549945154', 2, 10, 4, 10, 10000, '+8801683505006', 'Kalabagan, Dhaka-1205', '2019-02-28 10:00:00', '2019-12-28 10:00:00', 1, 0, '2019-02-05 18:23:55', NULL, NULL),
(32, '1D8B1540550051993', 3, 6, 2, 6, 1800, '1234', 'Kalabagan, Dhaka, Bangladesh', '2019-02-21 12:00:00', '2019-02-21 18:00:00', 1, 0, '2019-02-05 18:24:27', NULL, NULL),
(33, 'D5F01542640881373', 12, 7, 1, 1, 100, '1234', 'bnhgv ghjjhj hjhghj', '2018-12-25 12:00:00', '2018-12-25 13:00:00', 3, 0, '2018-11-26 06:57:01', '2018-11-26 00:00:00', '2018-11-26 00:00:00'),
(34, 'A8D91542642582610', 12, 8, 3, 4, 2400, '1234', 'jn jknkjnjknj jkknjjk', '2019-11-30 00:00:00', '2019-12-28 00:00:00', 2, 0, '2019-02-05 18:26:23', '2018-11-26 00:00:00', NULL),
(35, 'CC101542642772106', 12, 9, 4, 1, 1000, '1234', 'jkknn jkjkn kllkjnmkj', '2019-04-22 00:00:00', '2019-05-22 00:00:00', 2, 0, '2019-02-05 18:27:34', '2018-11-26 00:00:00', NULL),
(36, '22F51542642967814', 12, 10, 4, 5, 5000, '1234', 'hjjhjhkhghjkghbb hjnghjk', '2019-03-30 00:00:00', '2019-04-30 00:00:00', 2, 0, '2019-02-05 18:27:17', '2018-11-26 00:00:00', NULL),
(37, '9C581542643373252', 12, 6, 3, 4, 2400, '1234', 'mnmnn nmnmm nmnmnmn', '2019-11-29 00:00:00', '2019-12-27 00:00:00', 1, 0, '2019-02-05 18:27:51', NULL, NULL),
(39, '73791543212554758', 12, 11, 3, 6, 3600, '1234', 'dnf ksdnjf kjsfjn dskjsdnkf', '2019-11-30 00:00:00', '2019-12-28 00:00:00', 1, 0, '2019-02-05 18:28:32', NULL, NULL),
(40, 'F67D1548962738235', 1, 6, 1, 1, 100, '01710203040', 'chairman Accra Newtown junction', '2019-01-31 20:00:00', '2019-01-31 21:00:00', 1, 0, '2019-02-05 18:29:07', NULL, NULL),
(41, '84571542644510851', 12, 6, 3, 1, 600, '52421', 'ytgyuhjhg ughjghjnk', '2019-11-29 00:00:00', '2019-12-06 00:00:00', 1, 1, '2019-02-05 18:37:12', NULL, NULL),
(42, '96A51549392536779', 12, 12, 1, 1, 100, '1234', 'dhanmondi 27 ,Dhaka Bangladesh', '2019-02-07 02:00:00', '2019-02-07 03:00:00', 1, 0, '2019-02-05 18:48:56', NULL, NULL),
(43, '84571542644510851', 12, 6, 3, 1, 600, '52421', 'ytgyuhjhg ughjghjnk', '2018-11-29 00:00:00', '2018-12-06 00:00:00', 2, 1, '2019-02-06 16:39:12', NULL, NULL),
(44, '1DC31549638995617', 12, 17, 1, 1, 100, '1234', 'jxjxzhjzzk,jsjjsjz', '2019-02-21 16:00:00', '2019-02-21 05:00:00', 1, 0, '2019-02-08 15:16:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `payment_method` varchar(20) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_level`
--

CREATE TABLE `profile_level` (
  `id` int(11) NOT NULL,
  `level_name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_level`
--

INSERT INTO `profile_level` (`id`, `level_name`) VALUES
(1, 'Silver'),
(2, 'Gold'),
(3, 'Platinum');

-- --------------------------------------------------------

--
-- Table structure for table `request_order`
--

CREATE TABLE `request_order` (
  `id` int(11) NOT NULL,
  `order_number` varchar(17) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `book_structure_id` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `location` varchar(500) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `request_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pick_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_order`
--

INSERT INTO `request_order` (`id`, `order_number`, `customer_id`, `category_id`, `book_structure_id`, `duration`, `amount`, `phone`, `location`, `start_time`, `end_time`, `status`, `request_time`, `pick_time`) VALUES
(10, '2F531538640901295', 3, 1, 4, 10, 10000, '+8801683505006', 'Kalabagan, Dhaka-1205', '2018-10-15 00:00:00', '2019-08-15 00:00:00', 2, '2018-10-27 19:36:24', NULL),
(12, '7F5D1538640907136', 3, 3, 4, 10, 10000, '+8801683505006', 'Kalabagan, Dhaka-1205', '2018-11-01 00:00:00', '2019-09-01 00:00:00', 1, '2018-10-22 16:11:44', NULL),
(13, '60451538640911506', 3, 4, 1, 5, 10000, '+8801683505006', 'Kalabagan, Dhaka-1205', '2018-10-20 10:00:00', '2018-10-20 15:00:00', 2, '2018-11-05 17:22:08', NULL),
(14, 'E6EE1538640914809', 3, 5, 4, 12, 12000, '+8801683505006', 'Kalabagan, Dhaka-1205', '2018-10-10 00:00:00', '2019-10-10 00:00:00', 2, '2018-10-27 19:36:09', NULL),
(15, '63091538640917870', 3, 6, 4, 10, 10000, '+8801683505006', 'Kalabagan, Dhaka-1205', '2019-01-01 00:00:00', '2019-10-01 00:00:00', 1, '2018-10-22 16:11:38', NULL),
(16, 'DEB51538819600983', 3, 7, 4, 10, 10000, '+8801683505006', 'Kalabagan, Dhaka-1205', '2018-10-01 10:00:00', '2019-08-01 10:00:00', 1, '2018-10-22 16:11:36', NULL),
(17, '35C11538825986703', 4, 1, 1, 5, 500, '+8801683505006', 'bgdfhgdfhdfhdfhdfhdhdfh', '2018-10-06 20:00:00', '2018-10-06 01:00:00', 1, '2018-10-22 16:11:34', NULL),
(18, '436F1538826225003', 4, 3, 4, 1, 1000, '+8801683505006', '544555545454454545', '2018-10-06 00:00:00', '2018-11-05 00:00:00', 1, '2018-10-22 16:11:32', NULL),
(19, '5C6D1538826922077', 4, 2, 4, 1, 1000, '+8801683505006', 'hhfhhhhhhddfgccbn', '2018-10-06 00:00:00', '2018-11-05 00:00:00', 1, '2018-10-22 16:11:29', NULL),
(20, '4A111538835773318', 11, 1, 2, 6, 1800, '+8801683505006', 'Kalabagan, Dhaka', '2018-10-20 00:00:00', '2018-10-26 00:00:00', 1, '2018-10-22 16:11:27', NULL),
(21, '8D631538839061798', 11, 3, 4, 3, 3000, '+8801683505006', 'Kalabagan, Dhaka', '2018-10-20 00:00:00', '2019-01-18 00:00:00', 2, '2018-10-27 19:33:25', NULL),
(22, 'E9241538839123857', 11, 6, 2, 3, 900, '+8801683505006', 'Kalabagan, Dhaka', '2018-10-20 00:00:00', '2018-10-23 00:00:00', 2, '2018-10-27 19:32:40', NULL),
(23, '70F01538839228944', 11, 4, 1, 2, 200, '+8801683505006', 'Kalabagan, Dhaka', '2018-10-10 09:00:00', '2018-10-10 11:00:00', 2, '2018-10-27 19:30:07', NULL),
(25, '932315388949658EE', 11, 8, 2, 4, 1200, '01683505006', 'kalabagan, dhaka', '2018-10-13 00:00:00', '2018-10-17 00:00:00', 1, '2018-10-27 19:18:00', NULL),
(26, 'DAD31542644335285', 12, 1, 4, 6, 6000, '0141', 'ghjjktygh jhgjjk', '2018-11-29 00:00:00', '2019-05-28 00:00:00', 1, '2018-11-19 16:18:55', NULL),
(27, '84571542644510851', 12, 2, 3, 1, 600, '52421', 'ytgyuhjhg ughjghjnk', '2018-11-29 00:00:00', '2018-12-06 00:00:00', 2, '2019-02-05 17:12:50', NULL),
(28, '3F771549390048793', 12, 2, 1, 1, 100, '01638565654', '25 Lan desti, Palaman', '2019-02-09 03:00:00', '2019-02-09 04:00:00', 1, '2019-02-05 18:07:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `rate` float NOT NULL,
  `feedback` text NOT NULL,
  `review_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `order_id`, `rate`, `feedback`, `review_time`) VALUES
(1, 1, 3.5, 'Good Work', '2018-11-11 05:17:10'),
(2, 2, 4.5, 'Good Work but ...', '2018-09-30 15:18:47'),
(3, 1, 5, 'Good Work', '2018-09-24 11:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('g90Q2p1xBsRtnMmm9BnaJgroTvLEInMkvepW5O3M', 1, '103.196.235.42', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 'YTo0OntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiY2lWblVCUU5WSFFoNjVlYktMdzBLMWpETXJGTThBTFJjaFR0VTNUYiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzg6Imh0dHA6Ly9qaWJvLmluZml4c29mdC5jb20vcHVibGljL2FkbWluIjt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTt9', 1549720097);

-- --------------------------------------------------------

--
-- Table structure for table `support_complains`
--

CREATE TABLE `support_complains` (
  `id` int(11) NOT NULL,
  `order_number` varchar(17) DEFAULT NULL,
  `complain` longtext,
  `subject` varchar(200) DEFAULT NULL,
  `feedback` longtext,
  `type` tinyint(1) DEFAULT NULL COMMENT '0 for feedback1 for complain',
  `status` tinyint(1) DEFAULT NULL COMMENT '1 for active0 for deleted',
  `remarks` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support_complains`
--

INSERT INTO `support_complains` (`id`, `order_number`, `complain`, `subject`, `feedback`, `type`, `status`, `remarks`) VALUES
(1, NULL, NULL, 'Test Subject', 'Test feedback. Test feedback. Test feedback. Test feedback. ', 0, 1, NULL),
(2, NULL, NULL, 'User Interface', 'This app has an excellent user interface. It is easy to understand and use. Thank you JIBU team to bring this app to us. We appreciate the service of jibu also. all service from jibu are done well and right on time. I\'ll recommend others to use the service from JIBU. again thank you JIBU team.', 0, 1, NULL),
(3, '4RDA5678912355678', 'This app has an excellent user interface. It is easy to understand and use. Thank you JIBU team to bring this app to us. We appreciate the service of jibu also. all service from jibu are done well and right on time. I\'ll recommend others to use the service from JIBU. again thank you JIBU team.', NULL, NULL, 1, 1, 'This app has an excellent user interface. It is easy to understand');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_structure`
--
ALTER TABLE `book_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_index` (`customer_id`,`candidate_id`),
  ADD KEY `favorite_info_candidate_id_fk` (`candidate_id`);

--
-- Indexes for table `history_notification`
--
ALTER TABLE `history_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_notification_info_admin_id_fk` (`sender`);

--
-- Indexes for table `info_admin`
--
ALTER TABLE `info_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `info_candidate`
--
ALTER TABLE `info_candidate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `info_candidate_info_category_id_fk` (`category_id`),
  ADD KEY `info_candidate_profile_level_id_fk` (`level_id`),
  ADD KEY `info_candidate_office_info_id_fk` (`mobile`);

--
-- Indexes for table `info_category`
--
ALTER TABLE `info_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `info_customer`
--
ALTER TABLE `info_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_record_admin`
--
ALTER TABLE `login_record_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login_record_admin_info_admin_id_fk` (`admin_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `office_info`
--
ALTER TABLE `office_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `office_info_phone_uindex` (`phone`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_book_structure_id_fk` (`book_id`),
  ADD KEY `order_info_candidate_id_fk` (`candidate_id`),
  ADD KEY `order_info_customer_id_fk` (`customer_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_order_id_fk` (`order_id`);

--
-- Indexes for table `profile_level`
--
ALTER TABLE `profile_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_order`
--
ALTER TABLE `request_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `request_order_info_customer_id_fk` (`customer_id`),
  ADD KEY `request_order_info_category_id_fk` (`category_id`),
  ADD KEY `request_order_book_structure_id_fk` (`book_structure_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review_order_id_fk` (`order_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `support_complains`
--
ALTER TABLE `support_complains`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_structure`
--
ALTER TABLE `book_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `history_notification`
--
ALTER TABLE `history_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `info_admin`
--
ALTER TABLE `info_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `info_candidate`
--
ALTER TABLE `info_candidate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `info_category`
--
ALTER TABLE `info_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `info_customer`
--
ALTER TABLE `info_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `login_record_admin`
--
ALTER TABLE `login_record_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `office_info`
--
ALTER TABLE `office_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profile_level`
--
ALTER TABLE `profile_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `request_order`
--
ALTER TABLE `request_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `support_complains`
--
ALTER TABLE `support_complains`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `favorite`
--
ALTER TABLE `favorite`
  ADD CONSTRAINT `favorite_info_candidate_id_fk` FOREIGN KEY (`candidate_id`) REFERENCES `info_candidate` (`id`),
  ADD CONSTRAINT `favorite_info_customer_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `info_customer` (`id`);

--
-- Constraints for table `history_notification`
--
ALTER TABLE `history_notification`
  ADD CONSTRAINT `history_notification_info_admin_id_fk` FOREIGN KEY (`sender`) REFERENCES `info_admin` (`id`);

--
-- Constraints for table `info_candidate`
--
ALTER TABLE `info_candidate`
  ADD CONSTRAINT `info_candidate_info_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `info_category` (`id`),
  ADD CONSTRAINT `info_candidate_office_info_id_fk` FOREIGN KEY (`mobile`) REFERENCES `office_info` (`id`),
  ADD CONSTRAINT `info_candidate_profile_level_id_fk` FOREIGN KEY (`level_id`) REFERENCES `profile_level` (`id`);

--
-- Constraints for table `login_record_admin`
--
ALTER TABLE `login_record_admin`
  ADD CONSTRAINT `login_record_admin_info_admin_id_fk` FOREIGN KEY (`admin_id`) REFERENCES `info_admin` (`id`);

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_book_structure_id_fk` FOREIGN KEY (`book_id`) REFERENCES `book_structure` (`id`),
  ADD CONSTRAINT `order_info_candidate_id_fk` FOREIGN KEY (`candidate_id`) REFERENCES `info_candidate` (`id`),
  ADD CONSTRAINT `order_info_customer_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `info_customer` (`id`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_order_id_fk` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_order_id_fk` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
