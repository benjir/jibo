@extends('layouts.back')

@section('content')
  <div class="animated fadeIn" >
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="d-inline">
              Candidate List
            </div>
            <div class="d-inline" style="float:right">
              <a class="btn btn-sm btn-primary" href="{{route('candidates.create')}}">Add Candidate</a>
            </div>
          </div>
          <div class="card-body">
            @if (session('success'))
              <div class="alert alert-info">
                {{session('success')}}
              </div>
            @elseif (session('error'))
              <div class="alert alert-warning">
                {{session('error')}}
              </div>
            @endif

            <table class="table table-responsive-sm table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Mobile</th>
                  <th>Address</th>
                  <th>Gender</th>
                  <th>Badge</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($candidates as $candidate)
                  <tr>
                    <td>{{$candidate->name}}</td>
                    <td>{{$candidate->office->phone}}</td>
                    <td>{{$candidate->address}}</td>
                    <td>{{$candidate->gender}}</td>
                    <td>{{$candidate->level->level_name}}</td>
                    <td>
                      <span class="badge badge-{{$candidate->status ? 'success': 'danger'}}">{{$candidate->status ? 'Active' : 'Inactive'}}</span>
                    </td>
                    <td>
                      <a title="Show Canidate" class="btn btn-sm btn-info" href="{{route('candidates.show', ['id' => $candidate->id])}}">
                        <i class="fa fa-eye"></i>
                      </a>
                      <a title="Edit Canidate" class="btn btn-sm btn-warning" href="{{route('candidates.edit', ['id' => $candidate->id])}}">
                        <i class="fa fa-edit"></i>
                      </a>
                      <a title="Delete Canidate" class="btn btn-sm btn-danger" onclick="candidateDelete('{{route('candidates.destroy', ['id' => $candidate->id])}}')">
                        <i class="fa fa-trash"></i>
                      </a>
                    </td>
                  </tr>
                @empty

                @endforelse
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            {{$candidates->render()}}
          </div>
        </div>
      </div>
    </div>
    <!-- /.row-->
  </div>

  <div class="modal fade" id="candidateDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-danger" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are You Sure?</h4>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>It can be effected to related field. (Candidates log history)</p>
        </div>
        <div class="modal-footer">
          <form id="delete" action="" method="post">
            @csrf
            <input type="hidden" name="_method" value="DELETE">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </div>
      </div>
      <!-- /.modal-content-->
    </div>
    <!-- /.modal-dialog-->
  </div>
@endsection

@push('scripts')
  <script type="text/javascript">
    function candidateDelete(url) {
      var form = document.getElementById('delete');
      form.action = url;

      $("#candidateDelete").modal('show');
    }
  </script>
@endpush
