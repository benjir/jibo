@extends('layouts.back')
@section('content')
  <div class="animated fadeIn" style="box-shadow: 0 0 15px #cfcfcf">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header text-danger">
            <div class="d-inline">
              <strong>{{$candidate->name}}</strong>'s info
            </div>
            <div class="d-inline" style="float:right">
              <a class="btn btn-sm btn-primary" href="{{route('candidates.create')}}">Add Candidate</a>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <div class="card">
                  <img class="card-img-top" src="{{url('/')}}/JIBU/assets/candidates/{{$candidate->photo}}" alt="{{$candidate->name}}" width="100%">
                  <div class="card-body p-0">
                    <ul class="list-group">
                      {{-- <li class="list-group-item"> <h3>{{$candidate->name}}</h3> </li> --}}
                      <li class="list-group-item"> <span class="text-muted">Mobile:</span> {{$candidate->office->phone}} </li>
                      <li class="list-group-item"> <span class="text-muted">Gender:</span> {{$candidate->gender}} </li>
                      <li class="list-group-item"> <span class="text-muted">Badge:</span> {{$candidate->level->level_name}} </li>
                    </ul>
                  </div>
                  <div class="card-footer">
                    <a class="btn btn-sm btn-warning" href="{{route('candidates.edit', ['id' => $candidate->id])}}">
                      <i class="fa fa-edit"></i> Edit
                    </a>

                    <a class="btn btn-sm btn-danger" onclick="candidateDelete('{{route('candidates.destroy', ['id' => $candidate->id])}}')">
                      <i class="fa fa-trash"></i> Delete
                    </a>
                    <a class="btn btn-sm btn-secondary" onclick="document.getElementById('hide_candidate').submit()">
                      <i class="fa {{$candidate->status ? 'fa-eye-slash' : 'fa-eye'}}"></i> {{$candidate->status ? 'Hide' : 'Visible'}}
                    </a>
                    <form id="hide_candidate" action="{{route('candidates.hide', ['id' => $candidate->id])}}" method="post" style="display:none">
                      @csrf
                      <input type="hidden" name="_method" value="PUT">
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <h3>{{$candidate->name}}</h3>
                <hr>
                <table>
                  <tr>
                    <td class="text-muted">
                      Service Provide:
                    </td>
                    <td>
                      <span class="badge badge-danger">{{$candidate->service->name}}</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="text-muted">
                      Birth Day:
                    </td>
                    <td>
                      {{$candidate->birthdate}}
                    </td>
                  </tr>
                  <tr>
                    <td class="text-muted">
                      Height:
                    </td>
                    <td>
                      {{$candidate->height}}
                    </td>
                  </tr>
                  <tr>
                    <td class="text-muted">
                      Education:
                    </td>
                    <td>
                      {{$candidate->education_level}}
                    </td>
                  </tr>
                  <tr>
                    <td class="text-muted">
                      Address:
                    </td>
                    <td>
                      {{$candidate->address}}
                    </td>
                  </tr>
                  <tr>
                    <td class="text-muted">
                      Nationality:
                    </td>
                    <td>
                      {{$candidate->nationality}}
                    </td>
                  </tr>
                  <tr>
                    <td class="text-muted">
                      Language:
                    </td>
                    <td>
                      {{$candidate->language}}
                    </td>
                  </tr>
                  <tr>
                    <td class="text-muted">
                      Religion:
                    </td>
                    <td>
                      {{$candidate->religion}}
                    </td>
                  </tr>
                  <tr>
                    <td class="text-muted">
                      Status:
                    </td>
                    <td class="text-{{$candidate->status ? 'success': 'muted'}}">
                      {{$candidate->status ? 'active' : 'inactive'}}
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <i class="fa fa-align-justify"></i> Customers Order</div>
            <div class="card-body">
              <table class="table table-responsive-sm table-hover">
                <thead>
                  <tr>
                    <th>Customer</th>
                    <th>Book type</th>
                    <th>Duration</th>
                    <th>Amount</th>
                    <th>Phone</th>
                    <th>Location</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($orders as $order)
                    <tr>
                      <td>
                          <a title="Show Customer" class="link" href="{{route('customers.show', ['id' => $order->customer['id']])}}">
                            {{$order->customer['name']}}
                          </a>
                      </td>
                      <td>{{$order->book->name}}</td>
                      <td>{{$order->duration}}</td>
                      <td>{{$order->amount}}</td>
                      <td>{{$order->phone}}</td>
                      <td>{{$order->location}}</td>
                      <td>
                        <span class="badge badge-{{$order->status ? 'success': 'danger'}}">{{$order->status ? 'Active' : 'Inactive'}}</span>
                      </td>
                      <td>
                        {{-- <a title="Show Order" class="btn btn-sm btn-info" href="{{route('orders.show', ['id' => $order->id])}}">
                          <i class="fa fa-eye"></i>
                        </a> --}}
                        <a title="Edit Order" class="btn btn-sm btn-warning" href="{{route('orders.edit', ['id' => $order->id])}}">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a title="Delete Order" class="btn btn-sm btn-danger" href="{{route('orders.show', ['id' => $order->id])}}">
                          <i class="fa fa-trash"></i>
                        </a>
                      </td>
                    </tr>
                  @empty

                  @endforelse
                </tbody>
              </table>
              {{$orders->render()}}
            </div>
          </div>
      </div>
      <!-- /.col-->
    </div>
    <!-- /.row-->
  </div>



    <div class="modal fade" id="candidateDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-danger" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Are You Sure?</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <p>It can be effected to related field. (Candidates log history)</p>
          </div>
          <div class="modal-footer">
            <form id="delete" action="" method="post">
              @csrf
              <input type="hidden" name="_method" value="DELETE">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
              <button class="btn btn-danger" type="submit">Delete</button>
            </form>
          </div>
        </div>
        <!-- /.modal-content-->
      </div>
      <!-- /.modal-dialog-->
    </div>
  @endsection

  @push('scripts')
    <script type="text/javascript">
      function candidateDelete(url) {
        var form = document.getElementById('delete');
        form.action = url;

        $("#candidateDelete").modal('show');
      }
      function check() {
        var form = document.getElementById('delete');
        console.log(form.action);
      }
    </script>
  @endpush
