@extends('layouts.back')
@push('styles')
  <style media="screen">
    .user-list li{
      background: #cfcfcf;
      border-right: transparent;
    }
    .user-list .selected {
      background: #fff;
    }
    .chat-body .own {
      padding: 6px 20px;
      display: block;
      clear: both;
      margin-top: 3px;
      background: #efefef;
      border-radius: 50px;
      float: right;
    }

    .chat-body .other {
      padding: 6px 20px;
      display: block;
      clear: both;
      margin-top: 3px;
      float: left;
    }

    .chat-body .other .text {
      background: #c9e4f5;
      border-radius: 50px;
      display: inline-block;
      padding: 6px 20px;

    }
    .chat-body .other img {
      margin-right: .5rem;
      float: left;
      display: inline-block;
    }
  </style>
@endpush
@section('content')
  <div class="animated fadeIn" style="box-shadow: 0 0 15px #cfcfcf">
    <div class="bg-dark text-center p-3">
      Admin Chat Room For Support
    </div>
    <div class="row no-gutters">
      <div class="col-sm-12 col-md-4">
        <ul class="list-group user-list">
          <li class="bg-dark list-group-item">
            <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
            Baker Hasan
          </li>
          <li class="list-group-item  selected">
            <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
            Imran Chowdhwry
          </li>
          <li class="bg-dark list-group-item">
            <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
            Nazmul Hasan Arif
          </li>
          <li class="bg-dark list-group-item">
            <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
            Baker Hasan
          </li>
          <li class="bg-dark list-group-item">
            <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
            Imran Chowdhwry
          </li>
          <li class="bg-dark list-group-item">
            <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
            Nazmul Hasan Arif
          </li>
          <li class="bg-dark list-group-item">
            <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
            Baker Hasan
          </li>
          <li class="bg-dark list-group-item">
            <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
            Imran Chowdhwry
          </li>
          <li class="bg-dark list-group-item">
            <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
            Nazmul Hasan Arif
          </li>
        </ul>
      </div>
      <div class="col-sm-12 col-md-8">
        <div class="card" style="max-height: 500px; overflow: scroll; border-left: transparent; padding-bottom: 0;margin-bottom:0">
          <div class="card-body">
            <ul class="list-unstyled chat-body">
              <li class="own">
                Hello
              </li>
              <li class="other">
                <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
                <div class="text">
                  Hi
                </div>
              </li>
              <li class="own">How are you?</li>
              <li class="other">
                <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
                <div class="text">
                  What the hell is this?
                </div>
              </li>
              <li class="own">What about you?</li>
              <li class="other">
                <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
                <div class="text">
                  Who are you ?
                </div>
              </li>
              <li class="own">Where are you from?</li>
              <li class="other">
                <img src="https://via.placeholder.com/30x30"  class="rounded-circle">
                <div class="text">
                  Why are you bothering ?
                </div>
              </li>
              <li class="own">What do you do?</li>
            </ul>
          </div>
        </div>
        <form class="chat-message" action="" method="post">
          <textarea name="name" style="width:100%; height: 100%" placeholder="Reply"></textarea>
        </form>
      </div>
    </div>
    <!-- /.row-->
  </div>
@endsection
