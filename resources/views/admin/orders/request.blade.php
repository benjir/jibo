@extends('layouts.back')

@section('content')
  <div class="animated fadeIn" >
    <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <i class="fa fa-align-justify"></i> All Order</div>
              <div class="card-body">
                <table class="table table-responsive-sm table-hover">
                  <thead>
                    <tr>
                      <th>Service</th>
                      <th>Customer</th>
                      <th>Book type</th>
                      <th>Duration</th>
                      <th>Amount</th>
                      <th>Phone</th>
                      <th>Location</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($order_requests as $order_request)
                      <tr>
                        <td>
                            <a title="Show Service" class="link" href="{{route('services.show', ['id' => $order_request->service['id']])}}">
                              {{$order_request->service['name']}}
                            </a>
                        </td>
                        <td>
                            <a title="Show Customer" class="link" href="{{route('customers.show', ['id' => $order_request->customer['id']])}}">
                              {{$order_request->customer['name']}}
                            </a>
                        </td>
                        <td>{{$order_request->book->name}}</td>
                        <td>{{$order_request->duration}}</td>
                        <td>{{$order_request->amount}}</td>
                        <td>{{$order_request->phone}}</td>
                        <td>{{$order_request->location}}</td>
                        <td width='200'>
                          <span onclick="assignCandidate('{{$order_request->id }}')" style="cursor:pointer" class="badge badge-{{$order_request->status ==1? 'danger': 'success'}}">{{$order_request->status == 1? 'Not Assigned' : 'Assigned'}}</span>
                          {{-- <form id="assign-candidate-{{$order_request->id}}"  action="{{route('orders.assign')}}" method="post" style="display:none">
                              @csrf
                            <input type="hidden" name="order_id" value="{{$order_request->id}}" onchange="searchCandidate(this)">
                            <div class="input-group input-group-sm">
                                <input type="search" id="tag-candidate" name="candidate" value="" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1">
                                <div class="input-group-append">
                                    <button class="btn btn-success" type="submit">Assign</button>
                                </div>
                            </div>
                          </form> --}}
                        </td>
                        <td>
                            @if ($order_request->status != 2)
                                <a title="Delete Request" onclick="requestDelete('{{route('orders.request.destroy', ['id' => $order_request->id])}}')" style="cursor:pointer">
                                  <i class="fa fa-trash"></i>
                                </a>
                            @endif
                        </td>
                      </tr>
                    @empty
                    @endforelse
                  </tbody>
                </table>
                {{$order_requests->render()}}
              </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
  </div>

  <div class="modal fade" id="assignCandidate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-success" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Find Candidate To Assign</h4>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="$('#request_id').val('');$('#search').val('');$('#result').html('')">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="assign" action="{{route('orders.assign')}}" method="post">
                @csrf
                <input type="hidden" id="request_id" name="request_id" value="">
                <input type="search" id="search" name="candidate" value="" autocomplete="off">
                <ul class="list-group" id="result" style="display:none">

                </ul>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="$('#request_id').val('');$('#search').val('');$('#result').html('')">Close</button>
            <button class="btn btn-success" type="submit">Assign</button>
          </form>
        </div>
      </div>
      <!-- /.modal-content-->
    </div>
    <!-- /.modal-dialog-->
  </div>

  <div class="modal fade" id="requestDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-danger" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are You Sure?</h4>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>It will delete permanently</p>
        </div>
        <div class="modal-footer">
          <form id="delete" action="" method="post">
            @csrf
            @method('delete')
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </div>
      </div>
      <!-- /.modal-content-->
    </div>
    <!-- /.modal-dialog-->
  </div>
  @endsection

  @push('scripts')
  <script type="text/javascript">
    function requestDelete(url) {
      var form = document.getElementById('delete');
      form.action = url;

      $("#requestDelete").modal('show');
    }
  </script>
  @endpush


@push('scripts')
  <script type="text/javascript">
    function assignCandidate(id) {
      var form = document.getElementById('assign');
      // form.action = url;

      $('#request_id').val(id)


      $("#assignCandidate").modal('show');
    }

    $(function () {
        $(document).on('keyup', '#search', function () {
            $.ajax({
                method: "GET",
                url: "{{route('orders.candidate.find')}}",
                data: {
                    name: $(this).val(),
                    id: $('#request_id').val()
                },
                success: function (data) {
                    var list = '';
                    if (data.length) {
                        for (var i = 0; i < data.length; i++) {
                            list += `<li class="list-group-item" style="cursor:pointer" onclick="$('#search').val('${data[i]}'); $('#result').hide()">${data[i]}</li>`;
                        }
                        $('#result').show();
                        $('#result').html(list);
                    }
                }
            });
        })

    });

  </script>
@endpush
