@extends('layouts.back')

@section('content')
  <div class="animated fadeIn" >
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            Create New <strong>Service</strong>
          </div>
          <div class="card-body">
            @if (session('success'))
              <div class="alert alert-info">
                {{session('success')}}
              </div>
            @elseif (session('error'))
              <div class="alert alert-warning">
                {{session('error')}}
              </div>
            @endif
            <form action="{{route('services.store')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="name">Service Name</label>
                <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" name="name" value="{{ old('name') }}" placeholder="Enter Service Name..">
                @if ($errors->has('name'))
                  <span class="invalid-feedback help-block" role="alert">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
                @endif
              </div>
              <div class="form-group">
                <label for="nf-password">Icon</label>
                <input class="form-control-file {{ $errors->has('photo') ? ' is-invalid' : '' }}" type="file" name="photo">
                @if ($errors->has('photo'))
                  <span class="invalid-feedback help-block" role="alert">
                      <strong>{{ $errors->first('photo') }}</strong>
                  </span>
                @endif
              </div>
              <div class="form-group">
                <label for="nf-password">Big Photo</label>
                <input class="form-control-file{{ $errors->has('photo_hi') ? ' is-invalid' : '' }}" type="file" name="photo_hi">
                @if ($errors->has('photo_hi'))
                  <span class="invalid-feedback help-block" role="alert">
                      <strong>{{ $errors->first('photo_hi') }}</strong>
                  </span>
                @endif
              </div>

          </div>
          <div class="card-footer">
            <button class="btn btn-sm btn-primary" type="submit">
              <i class="fa fa-dot-circle-o"></i> Submit</button>
            <button class="btn btn-sm btn-danger" type="reset">
              <i class="fa fa-ban"></i> Reset</button>
              </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row-->
  </div>
@endsection
