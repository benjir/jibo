@extends('layouts.back')

@section('content')
  <div class="animated fadeIn" >
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            Edit <strong>{{$service->name}}</strong>
          </div>
          <div class="card-body">
            @if (session('success'))
              <div class="alert alert-info">
                {{session('success')}}
              </div>
            @elseif (session('error'))
              <div class="alert alert-warning">
                {{session('error')}}
              </div>
            @endif
            <form action="{{route('services.update', ['id' => $service->id])}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_method" value="put">
              @csrf
              <div class="form-group">
                <label for="name">Service Name</label>
                <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" name="name" value="{{ $service->name }}" placeholder="Enter Service Name..">
                @if ($errors->has('name'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
                @endif
              </div>
              <div class="form-group">
                <label for="nf-password">Icon</label>
                <img src="{{url('/')}}/JIBU/assets/categories/{{$service->photo}}" width="30px" alt="">
                <input class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" type="file" name="photo">
                @if ($errors->has('photo'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('photo') }}</strong>
                  </span>
                @endif
              </div>
              <div class="form-group">
                <label for="nf-password">Big Photo</label>
                <img src="{{url('/')}}/JIBU/assets/categories/{{$service->photo_hi}}" width="60px" alt="">
                <input class="form-control{{ $errors->has('photo_hi') ? ' is-invalid' : '' }}" type="file" name="photo_hi">
                @if ($errors->has('photo_hi'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('photo_hi') }}</strong>
                  </span>
                @endif
              </div>

          </div>
          <div class="card-footer">
            <button class="btn btn-sm btn-primary" type="submit">
              <i class="fa fa-dot-circle-o"></i> Submit</button>
            <button class="btn btn-sm btn-danger" type="reset">
              <i class="fa fa-ban"></i> Reset</button>
              </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row-->
  </div>
@endsection
