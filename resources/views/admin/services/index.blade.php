@extends('layouts.back')

@section('content')
  <div class="animated fadeIn" >
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="card">
          <div class="card-body  text-secondary text-center">
            <div class="h1 mb-4">
              <i class="icon-plus"></i>
            </div>
            <div class="text-value">
              <a href="{{route('services.create')}}" class="text-secondary">
                Add Service
              </a>
            </div>
          </div>
        </div>
      </div>
        @php
          $color = [
            'info',
            'warning',
            'success',
            'danger',
            'primary'
          ];
          $i=0;
        @endphp

        @foreach ($services as $service)
          <div class="col-sm-6 col-md-3">
            <div class="card">
              <div class="card-body text-{{$color[$i]}} text-center">
                <div class="h1 mb-4" >
                  {{-- <i class="icon-user-follow"></i> --}}
                  <img src="{{url('/')}}/JIBU/assets/categories/{{$service->photo}}" alt="">
                </div>
                <div class="text-value">
                  <a href="{{route('services.show', ['id' => $service->id])}}" class="text-{{$color[$i]}}">
                    {{$service->name}}
                  </a>
                </div>
              </div>
            </div>
          </div>
        @php
        $i++;
         if (empty($color[$i])) {
           $i = 0;
         }
        @endphp
        @endforeach
        <!-- /.col-->
      </div>
    <!-- /.row-->
  </div>
@endsection
