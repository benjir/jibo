@extends('layouts.back')

@section('content')
  <div class="animated fadeIn" style="box-shadow: 0 0 15px #cfcfcf">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header text-danger">
            <img src="{{url('/')}}/JIBU/assets/categories/{{$service->photo}}" alt="{{$service->name}}" width="30px">
            <strong>{{$service->name}}</strong>'s info
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <div class="card">
                  <img class="card-img-top" src="{{url('/')}}/JIBU/assets/categories/{{$service->photo_hi}}" alt="{{$service->name}}" width="100%">
                </div>
              </div>
              <div class="col-md-8">
                <h3>{{$service->name}}</h3>
                <hr>
                <div>
                  <strong>total candidates: </strong>{{count($candidates)}}
                </div>
                <hr>
                <div>
                  <a class="btn btn-sm btn-warning" href="{{route('services.edit', ['id' => $service->id])}}">
                    <i class="fa fa-edit"></i> Edit
                  </a>

                  <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#serviceDelete"
                      href="{{route('services.edit', ['id' => $service->id])}}">
                    <i class="fa fa-trash"></i> Delete
                  </a>
                  <a class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#serviceDelete"
                      href="{{route('services.edit', ['id' => $service->id])}}">
                    <i class="fa fa-eye-slash"></i> Hide
                  </a>
                </div>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i> Candidates</div>
                  <div class="card-body">
                    <table class="table table-responsive-sm table-hover">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Mobile</th>
                          <th>Address</th>
                          <th>Gender</th>
                          <th>Badge</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @forelse ($candidates as $candidate)
                          <tr>
                            <td>{{$candidate->name}}</td>
                            <td>{{$candidate->office->phone}}</td>
                            <td>{{$candidate->address}}</td>
                            <td>{{$candidate->gender}}</td>
                            <td>{{$candidate->level->level_name}}</td>
                            <td>
                              <span class="badge badge-{{$candidate->status ? 'success': 'danger'}}">{{$candidate->status ? 'Active' : 'Inactive'}}</span>
                            </td>
                            <td>
                              <a title="Show Canidate" class="btn btn-sm btn-info" href="{{route('candidates.show', ['id' => $candidate->id])}}">
                                <i class="fa fa-eye"></i>
                              </a>
                              <a title="Edit Canidate" class="btn btn-sm btn-warning" href="{{route('candidates.edit', ['id' => $candidate->id])}}">
                                <i class="fa fa-edit"></i>
                              </a>
                              <a title="Delete Canidate" class="btn btn-sm btn-danger" href="{{route('candidates.show', ['id' => $candidate->id])}}">
                                <i class="fa fa-trash"></i>
                              </a>
                            </td>
                          </tr>
                        @empty

                        @endforelse
                      </tbody>
                    </table>
                    {{$candidates->render()}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.col-->
    </div>
    <!-- /.row-->
  </div>


  <div class="modal fade" id="serviceDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-danger" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are You Sure?</h4>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>It will be effected to related field. (Candidate)</p>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          <button class="btn btn-danger" type="button">Delete</button>
        </div>
      </div>
      <!-- /.modal-content-->
    </div>
    <!-- /.modal-dialog-->
  </div>

@endsection
