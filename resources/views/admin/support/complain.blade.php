@extends('layouts.back')

@section('content')
  <div class="animated fadeIn" >
    <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <i class="fa fa-align-justify"></i> All Complains</div>
              <div class="card-body">
                <table class="table table-responsive-sm table-hover">
                  <thead>
                    <tr>
                      <th>Order Number</th>
                      <th>Complain</th>
                      <th>Subject</th>
                      <th>Status</th>
                      <th>Remarks</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($complains as $complain)
                      <tr>
                        <td>
                            {{$complain->order_number}}
                        </td>
                        <td>
                            {{$complain->complain}}
                        </td>
                        <td>{{$complain->subject or ''}}</td>
                        <td><span class="badge badge-{{$complain->status ? 'success': 'danger'}}">{{$complain->status ? 'Active' : 'Inactive'}}</span></td>
                        <td>{{$complain->remarks}}</td>
                      </tr>
                    @empty

                    @endforelse
                  </tbody>
                </table>
                {{$complains->render()}}
              </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
  </div>

@endsection
