@extends('layouts.back')

@section('content')
  <div class="animated fadeIn" >
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            Edit <strong>Candidate | </strong> <em>{{$candidate->name}}</em>
          </div>
          <div class="card-body">
            @if (session('success'))
              <div class="alert alert-info">
                {{session('success')}}
              </div>
            @elseif (session('error'))
              <div class="alert alert-warning">
                {{session('error')}}
              </div>
            @endif
            <form action="{{route('candidates.update', ['id' => $candidate->id])}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="_method" value="PUT">
              <div class="row">
                <div class="col-md-6">
                  <div class="card-body"  style="background: #fafafa; padding:1rem">
                    <div class="form-group">
                      <label class="font-weight-bold">Candidate Name</label>
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" name="name" value="{{ $candidate->name }}" placeholder="Enter Candidate Name..">
                      @if ($errors->has('name'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="font-weight-bold">Candidate Address</label>
                      <input class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" type="text" name="address" value="{{ $candidate->address }}" placeholder="Enter Candidate Address..">
                      @if ($errors->has('address'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="font-weight-bold">Select Gender</label>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="gender" value="male" {{$candidate->gender == 'male' ? 'checked': ''}}>
                        <label class="form-check-label" for="inlineCheckbox2">Male</label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="gender" value="female" {{$candidate->gender == 'female' ? 'checked': ''}}>
                        <label class="form-check-label" for="inlineCheckbox3">Female</label>
                      </div>
                      @if ($errors->has('gender'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card-body"  style="background: #fafafa; padding:1rem">
                    <div class="form-group">
                      <label class="font-weight-bold">Nationality</label>
                      <input class="form-control{{ $errors->has('nationality') ? ' is-invalid' : '' }}" id="nationality" type="text" name="nationality" value="{{ $candidate->nationality }}" placeholder="Enter Candidate Nationality..">
                      @if ($errors->has('nationality'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('nationality') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="font-weight-bold">Education Level</label>
                      <input class="form-control{{ $errors->has('education_level') ? ' is-invalid' : '' }}" id="education_level" type="text" name="education_level" value="{{ $candidate->education_level }}" placeholder="Enter Candidate Education..">
                      @if ($errors->has('education_level'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('education_level') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="font-weight-bold">Religion</label>
                      <input class="form-control{{ $errors->has('religion') ? ' is-invalid' : '' }}" id="religion" type="text" name="religion" value="{{ $candidate->religion }}" placeholder="Enter Candidate Religion..">
                      @if ($errors->has('religion'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('religion') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="font-weight-bold">Language</label>
                      <input class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }}" id="language" type="text" name="language" value="{{ $candidate->language }}" placeholder="Enter Candidate Language..">
                      @if ($errors->has('language'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('language') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="col-md-6 mt-2">
                  <div class="card-body"  style="background: #fafafa; padding:1rem">
                    <div class="form-group">
                      <label class="font-weight-bold">Assign Service</label>
                      <select class="custom-select" name="category_id">
                        <option selected value="null">Select Service</option>
                        @foreach ($services as $service)
                          <option value="{{$service->id}}" {{$service->id == $candidate->service->id ? 'selected':''}}>{{$service->name}}</option>
                        @endforeach
                      </select>
                      @if ($errors->has('category_id'))
                        <span class="invalid-feedback help-block" role="alert" style="display:block">
                            <strong>{{ $errors->first('category_id') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="font-weight-bold">Set Level</label>
                      <select class="custom-select" name="level_id">
                        <option selected value="null">Select Level</option>
                        @foreach ($levels as $level)
                          <option value="{{$level->id}}" {{$level->id == $candidate->level->id ? 'selected':''}}>{{$level->level_name}}</option>
                        @endforeach
                      </select>
                      @if ($errors->has('level_id'))
                        <span class="invalid-feedback help-block" role="alert" style="display:block">
                            <strong>{{ $errors->first('level_id') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="font-weight-bold">Defalut Status</label>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" value="1" {{$candidate->status ? 'checked':''}}>
                        <label class="form-check-label" for="inlineCheckbox2">Active</label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" value="0" {{$candidate->status ? '':'checked'}}>
                        <label class="form-check-label" for="inlineCheckbox2">Inactive</label>
                      </div>
                      @if ($errors->has('status'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="col-md-6 mt-2">
                  <div class="card-body"  style="background: #fafafa; padding:1rem">
                    <div class="form-group">
                      <label class="font-weight-bold">Birth Date</label>
                      <input class="form-control{{ $errors->has('birthdate') ? ' is-invalid' : '' }}" id="birthdate" type="text" name="birthdate" value="{{ \Carbon\Carbon::parse($candidate->birthdate)->format('m/d/Y') }}" >
                      @if ($errors->has('birthdate'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('birthdate') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="font-weight-bold">Height</label>
                      <input class="form-control{{ $errors->has('height') ? ' is-invalid' : '' }}" id="height" type="text" name="height" value="{{ $candidate->height }}" placeholder="5'6">
                      @if ($errors->has('height'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('height') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label class="d-block font-weight-bold">Candidate Photo</label>
                      <img src="{{url('/JIBU/assets/candidates/').'/'.$candidate->photo}}" width="50px" alt="">
                      <input class="form-control-file{{$errors->has('photo') ? ' is-invalid' : '' }}" type="file" name="photo">
                      @if ($errors->has('photo'))
                        <span class="invalid-feedback help-block" role="alert">
                            <strong>{{ $errors->first('photo') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="card-footer">
            <button class="btn  btn-primary" type="submit">
              <i class="fa fa-dot-circle-o"></i> Update Candidte</button>
            <button class="btn  btn-danger" type="reset">
              <i class="fa fa-ban"></i> Reset</button>
              </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row-->
  </div>
@endsection
