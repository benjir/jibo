@extends('layouts.back')

@section('content')
  <div class="animated fadeIn" >
    <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <i class="fa fa-align-justify"></i> All Order</div>
              <div class="card-body">
                <table class="table table-responsive-sm table-hover">
                  <thead>
                    <tr>
                      <th>Candidate</th>
                      <th>Customer</th>
                      <th>Book type</th>
                      <th>Duration</th>
                      <th>Amount</th>
                      <th>Phone</th>
                      <th>Location</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($orders as $order)
                      <tr>
                        <td>
                            <a title="Show Candidate" class="link" href="{{route('candidates.show', ['id' => $order->candidate['id']])}}">
                              {{$order->candidate['name']}}
                            </a>
                        </td>
                        <td>
                            <a title="Show Customer" class="link" href="{{route('customers.show', ['id' => $order->customer['id']])}}">
                              {{$order->customer['name']}}
                            </a>
                        </td>
                        <td>{{$order->book->name}}</td>
                        <td>{{$order->duration}}</td>
                        <td>{{$order->amount}}</td>
                        <td>{{$order->phone}}</td>
                        <td>{{$order->location}}</td>
                        <td>
                          <span class="badge badge-{{$order->status ? 'success': 'danger'}}">{{$order->status ? 'Active' : 'Inactive'}}</span>
                        </td>
                      </tr>
                    @empty

                    @endforelse
                  </tbody>
                </table>
                {{$orders->render()}}
              </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
  </div>

@endsection
