@extends('layouts.back')

@section('content')
  <div class="animated fadeIn">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <i class="fa fa-align-justify"></i> Candidates</div>
            <div class="card-body">
              <table class="table table-responsive-sm table-hover">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date registered</th>
                    <th>Badge</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Samppa Nori</td>
                    <td>this.user@gmail.com</td>
                    <td>2012/01/01</td>
                    <td>Silver</td>
                    <td>
                      <span class="badge badge-success">Active</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Samppa Nori</td>
                    <td>this.user@gmail.com</td>
                    <td>2012/01/01</td>
                    <td>Bronge</td>
                    <td>
                      <span class="badge badge-secondary">inactive</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Samppa Nori</td>
                    <td>this.user@gmail.com</td>
                    <td>2012/01/01</td>
                    <td>Gold</td>
                    <td>
                      <span class="badge badge-success">Active</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Samppa Nori</td>
                    <td>this.user@gmail.com</td>
                    <td>2012/01/01</td>
                    <td>Silver</td>
                    <td>
                      <span class="badge badge-success">Active</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Samppa Nori</td>
                    <td>this.user@gmail.com</td>
                    <td>2012/01/01</td>
                    <td>Bronge</td>
                    <td>
                      <span class="badge badge-secondary">inactive</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Samppa Nori</td>
                    <td>this.user@gmail.com</td>
                    <td>2012/01/01</td>
                    <td>Gold</td>
                    <td>
                      <span class="badge badge-success">Active</span>
                    </td>
                  </tr>
                </tbody>
              </table>
              <ul class="pagination">
                <li class="page-item">
                  <a class="page-link" href="#">Prev</a>
                </li>
                <li class="page-item active">
                  <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">3</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">4</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">Next</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- /.row-->
    </div>
@endsection
