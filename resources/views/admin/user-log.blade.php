@extends('layouts.back')

@push('styles')
<style media="screen" scoped>
.profile-activity {
  padding: 10px 4px;
  border-bottom: 1px dotted #D0D8E0;
  position: relative;
  border-left: 1px dotted #FFF;
  border-right: 1px dotted #FFF
}

.profile-activity:first-child {
  border-top: 1px dotted transparent
}

.profile-activity:first-child:hover {
  border-top-color: #D0D8E0
}

.profile-activity:hover {
  background-color: #F4F9FD;
  border-left: 1px dotted #D0D8E0;
  border-right: 1px dotted #D0D8E0
}

.profile-activity img {
  border: 2px solid #C9D6E5;
  border-radius: 100%;
  max-width: 40px;
  margin-right: 10px;
  margin-left: 0;
  box-shadow: none
}

.profile-activity .thumbicon {
  background-color: #74ABD7;
  display: inline-block;
  border-radius: 100%;
  width: 38px;
  height: 38px;
  color: #FFF;
  font-size: 18px;
  text-align: center;
  line-height: 38px;
  margin-right: 10px;
  margin-left: 0;
  text-shadow: none!important
}

.profile-activity .time {
  display: block;
  margin-top: 4px;
  color: #777
}

.profile-activity a.user {
  font-weight: 700;
  color: #9585BF
}

.profile-activity .tools {
  position: absolute;
  right: 12px;
  bottom: 8px;
  display: none
}

.profile-activity:hover .tools {
  display: block
}

.user-profile .ace-thumbnails li {
  border: 1px solid #CCC;
  padding: 3px;
  margin: 6px
}

.user-profile .ace-thumbnails li .tools {
  left: 3px;
  right: 3px
}

.user-profile .ace-thumbnails li:hover .tools {
  bottom: 3px
}

.user-title-label:hover {
  text-decoration: none
}

.user-title-label+.dropdown-menu {
  margin-left: -12px
}

.profile-contact-links {
  padding: 4px 2px 5px;
  border: 1px solid #E0E2E5;
  background-color: #F8FAFC
}

.btn-link:hover .ace-icon {
  text-decoration: none!important
}
</style>
@endpush

@section('content')
  <div class="animated fadeIn" style="box-shadow: 0 0 15px #cfcfcf">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">{{$user->name. "'s"}} Activity</div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <div class="card">
                  <img class="card-img-top" src="https://via.placeholder.com/350x150" alt="Card image cap">
                  <div class="card-body" style="padding:0px 0px">
                    <ul class="list-group">
                      <li class="list-group-item">
                        <span class="text-secondary">Name:</span>
                        <strong>{{$user->name}}</strong> </li>
                      <li class="list-group-item">
                        <span class="text-secondary">User since :</span>
                        {{\Carbon\Carbon::parse($user->joining_date)->diffForHumans()}}
                      </li>
                      <li class="list-group-item">
                        <span class="text-secondary">
                          Last Activity:
                        </span>
                        {{$user->online ? \Carbon\Carbon::parse($user->online->last_activity)->diffForHumans() : ''}}
                      </li>
                      <li class="list-group-item">
                        <span class="text-secondary">Last Login:</span>
                        {{$user->login()->first() ? \Carbon\Carbon::parse($user->login()->first()->login_time)->diffForHumans() : ''}}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="card">
                  <div class="card-header">
                    User Log Activities
                  </div>
                  <div class="card-body">

                    @forelse ($user->log as $log)
                      <div class="profile-activity clearfix">
        								<div>

        								</div>
                        @if (!is_null($log->login_time))
                          <img class="pull-left" alt="Alex Doe's avatar" src="http://bootdey.com/img/Content/avatar/avatar4.png">
        									<a class="user" href="#"> {{$user->name}} </a>
        									   logged in.
        									<div class="time">
        										<i class="ace-icon fa fa-clock-o bigger-110"></i>
        										{{\Carbon\Carbon::parse($log->login_time)->toDayDateTimeString()}}
        									</div>
                        @else
                          <img class="pull-left" alt="Alex Doe's avatar" src="http://bootdey.com/img/Content/avatar/avatar4.png">
        									<a class="user" href="#"> {{$user->name}} </a>
        									   logged out.
        									<div class="time">
        										<i class="text-danger ace-icon fa fa-clock-o bigger-110"></i>
        										{{\Carbon\Carbon::parse($log->logout_time)->toDayDateTimeString()}}
        									</div>
                        @endif
        							</div>
                    @empty
                      <div class="profile-activity clearfix">
        								<div>
        									log data not found
        								</div>
        							</div>
                    @endforelse

                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.col-->
    </div>
    <!-- /.row-->
  </div>
@endsection
