@extends('layouts.dashboard')

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-align-justify"></i> User Data Table</div>
        <div class="card-body">
          <table class="datatable table table-responsive-sm table-bordered table-striped table-sm">
            <thead>
              <tr>
                <th># </th>
                <th>Name</th>
                <th>Email</th>
                <th>Total Article</th>
                <th>Role</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              @php
                $i = 0;
              @endphp
              @forelse ($users as $user)
                <tr>
                  <td>{{++$i}}</td>
                  <td>
                    <a style="color:black" href="{{route('admin.users.show', ['id' => $user->id])}}">
                     {{$user->name}}
                    </a>
                  </td>
                  <td>
                    <a style="color:black" href="{{route('admin.users.show', ['id' => $user->id])}}">
                     {{$user->email}}
                    </a>
                  </td>
                  <td>{{$user->articles->count()}}</td>
                  <td>{{$user->roles()->first()->name}}</td>
                  <td>{{$user->status}}</td>
                </tr>
              @empty
                No Data Found!!!
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- /.col-->
  </div>
  <!-- /.row-->
@endsection
@push('scripts')
  <script type="text/javascript">
    $(document).ready(function (){
       var table = $('.datatable').DataTable({
          'columnDefs': [
             {
                'targets': 0,
                'checkboxes': {
                   'selectRow': true
                }
             }
          ],
          'select': {
             'style': 'multi'
          },
          'order': [[1, 'asc']]
       });
       // // Handle form submission event
       //   $('#frm-example').on('submit', function(e){
       //      var form = this;
       //
       //      var rows_selected = table.column(0).checkboxes.selected();
       //
       //      // Iterate over all selected checkboxes
       //      $.each(rows_selected, function(index, rowId){
       //         // Create a hidden element
       //         $(form).append(
       //             $('<input>')
       //                .attr('type', 'hidden')
       //                .attr('name', 'id[]')
       //                .val(rowId)
       //         );
       //      });
       //   });
    });
  </script>
@endpush
