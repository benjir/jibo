@extends('layouts.dashboard')
@section('content')
  <section class="content">
      <div class="row">
          <div class="col-xs-12">
              <div class="box">
                  <div class="box-header">
                      <h3  class="box-title">Article | tagged by  <span style="color:Tomato">{{$tag->name}}</span> </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      <table id="example1" class="table table-bordered table-hover datatable2">
                          <thead>
                            <tr>
                              <th># </th>
                              <th>Author</th>
                              <th>Title</th>
                              <th>Content</th>
                              <th>Tags</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @php
                              $i = $articles->perPage() * ($articles->currentPage()-1)
                            @endphp
                            @forelse ($articles as $article)
                              <tr id="article_{{$article->id}}">
                                <td>{{++$i}}</td>
                                <td>
                                  <a style="color:black" href="{{route('admin.articles.show', ['id' => $article->id])}}">
                                   {{$article->user->name}}
                                  </a>
                                </td>
                                <td>
                                   <a style="color:black" href="{{route('admin.articles.show', ['id' => $article->id])}}">
                                   {{$article->title}}
                                  </a>
                                </td>
                                <td>{{$article->content}}</td>
                                <td>{{'empty'}}</td>
                                <td>{{$article->status}}</td>
                                <td>
                                  <a style="color:Tomato;cursor:pointer" onclick="DeleteModel('article_{{$article->id}}', '{{route('admin.articles.destroy', ['id' => $article->id])}}');"> <i class="fa fa-trash-o"></i> </a>
                                  <a style="color:#fab005" href="{{route('admin.articles.show', ['id' => $article->id])}}"> <i class="fa fa-edit"></i> </a>
                                  <a style="color:#72c3fc"  href="{{route('admin.articles.show', ['id' => $article->id])}}"> <i class="fa fa-eye"></i> </a>
                                </td>
                              </tr>
                            @empty
                              No Data Found!!!
                            @endforelse
                          </tbody>
                      </table>
                      {{$articles->render()}}
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->
              <div class="box">
                  <div class="box-header">
                      <h3  class="box-title">Media | tagged by  <span style="color:Tomato">{{$tag->name}}</span> </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      <div class="row">
                        @foreach ($media as $med)
                          <div id="media_{{$med->id}}" class="col-md-3 col-sm-6 col-xs-12">
                            <a onclick="DeleteModel('media_{{$med->id}}', '{{route('admin.media.destroy', ['id' => $med->id])}}');" class="btn btn-sm" style="cursor:pointer" >Delete</a>
                            <img data-action="zoom" class="img-responsive" src="{{url('/')}}/media-images/images/{{$med->url}}" alt="{{$med->name}}">
                          </div>
                        @endforeach
                        <div class="col-md-12">
                          {{$media->render()}}
                        </div>
                      </div>
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->
              <div class="box">
                  <div class="box-header">
                      <h3  class="box-title">User | tagged by  <span style="color:Tomato">{{$tag->name}}</span> </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      <table id="example1" class="table table-bordered table-hover datatable2">
                          <thead>
                            <tr>
                              <th># </th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Total Article</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @php
                              $i = $users->perPage() * ($users->currentPage()-1)
                            @endphp
                            @forelse ($users as $user)
                              <tr>
                                <td>{{++$i}}</td>
                                <td>
                                  <a style="color:black" href="{{route('admin.users.show', ['id' => $user->id])}}">
                                   {{$user->name}}
                                  </a>
                                </td>
                                <td>
                                  <a style="color:black" href="{{route('admin.users.show', ['id' => $user->id])}}">
                                   {{$user->email}}
                                  </a>
                                </td>
                                <td>{{$user->articles->count()}}</td>
                                <td>{{$user->status}}</td>
                                <td>
                                  <a style="color:#72c3fc"  href="{{route('admin.users.show', ['id' => $user->id])}}"> <i class="fa fa-eye"></i> </a>
                                </td>
                              </tr>
                            @empty
                              No Data Found!!!
                            @endforelse
                          </tbody>
                      </table>
                      {{$users->render()}}
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->
          </div>
          <!-- /.col -->
      </div>
      <!-- /.row -->
  </section>
  <!-- /. main content -->
@endsection
@push('scripts')
	<script type="text/javascript">
      function DeleteModel(model, route) {
        if (!confirm('Are you sure?')) {
          return;
        }
        var _token = document.head.querySelector("[name=csrf-token]").content;
        console.log(route);
        return;
        $.ajax({
          url:route,
          type: 'post',
          data: {_method: 'delete', _token : _token},
          success:function(msg){
            alert(msg.text);
            $('#'+model).remove();
          },
          error:function(error){
            alert('Something Wrong!!!');
          }
        });
      }
  </script>
@endpush
