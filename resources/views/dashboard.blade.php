@extends('layouts.back')


@section('content')
  <div class="animated fadeIn" style="box-shadow: 0 0 15px #cfcfcf">
    <div class="row"  >
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Activities</div>
          <div class="card-body">
            <!-- /.row-->
            <table class="table table-responsive-sm table-hover mb-0">
              <thead>
                <tr>
                  <th class="text-center">
                    <i class="icon-people"></i>
                  </th>
                  <th>User</th>
                  <th class="text-center">Role</th>
                  <th>Status</th>
                  <th>Activities</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($all_staff as $staff)
                  <tr class="{{$staff->id == auth()->user()->id ? 'text-secondary' : ''}}">
                    <td class="text-center">
                      <div class="avatar">
                        <img class="img-avatar" src="{{url('/')}}/coreui/img/avatars/1.jpg" alt="admin@bootstrapmaster.com">
                        <span class="avatar-status {{$staff->online ? 'badge-success' : 'badge-danger'}}"></span>
                      </div>
                    </td>
                    <td>
                      <div>
                        <a href="{{route('log', ['id' => $staff->id])}}">{{$staff->name}}</a>
                      </div>
                      {{-- <div class="small text-muted">
                        <span>New</span> | Registered: Jan 1, 2015</div> --}}
                    </td>
                    <td class="text-center">
                      {{ $staff->role() }}
                    </td>
                    <td>
                      <span class="badge badge-{{$staff->status ? 'success' : 'secondary'}}">{{$staff->status ? 'Active' : 'Inactive'}}</span>
                    </td>
                    <td>
                      <div class="small text-muted">Last Activity</div>
                      <strong>{{$staff->online ? \Carbon\Carbon::parse($staff->online->last_activity)->diffForHumans() : ''}}</strong>
                    </td>
                    <td>
                      <a class="text-success" href="{{route('log', ['id' => $staff->id])}}"><i class="fa fa-eye"></i></a>
                      &nbsp;
                      <i class="fa fa-pencil-square-o"></i>
                      &nbsp;
                      <i class="fa fa-trash-o"></i>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-->
    </div>
    <!-- /.row-->
  </div>
@endsection
