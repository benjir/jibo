<!DOCTYPE html>

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="BDPEDIA - Open Source Wiki Article">
    <meta name="author" content="Baker Hasan">
    <meta name="keyword" content="bdpedia,wiki,wikipedia,bdmedia,bdwiki,wikibd">
    <title>JIBO - Application</title>
    <!-- Icons-->
    <link href="{{url('/')}}/coreui/vendors/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <link rel="stylesheet" href="{{url('/')}}/coreui/vendors/datatable/datatables.min.css">
    <link rel="stylesheet" href="{{url('/')}}/coreui/vendors/datatable/dataTables.checkboxes.css">
    <!-- Main styles for this application-->
    <link href="{{url('/')}}/coreui/css/style.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    @stack('styles')
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img src="{{url('/')}}/assets/jibulogo-screen.png" alt="" height="75%" >
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
          <a class="nav-link" href="{{route('chat')}}">
            <i class="icons font-2xl cui-speech"></i>
            <span class="badge badge-pill badge-danger">5</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img class="img-avatar" src="{{url('/')}}/coreui/img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
              <i class="fa fa-lock"></i> Logout</a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
          </div>
        </li>
      </ul>
    </header>
    <div class="app-body">


      <!--
      | sidebar start
      -->
      <div class="sidebar">
        <nav class="sidebar-nav">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="{{route('dashboard')}}">
              <i class="nav-icon icon-speedometer"></i> Dashboard
                {{-- <span class="badge badge-primary">NEW</span> --}}
              </a>
            </li>

            {{-- <li class="nav-title" style="color:#20a8d8">Contents</li> --}}

            <li class="nav-item">
              <a class="nav-link" href="{{route('services.index')}}">
              <i class="nav-icon icon-speedometer"></i> Services
                {{-- <span class="badge badge-primary">NEW</span> --}}
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('candidates.index')}}">
              <i class="nav-icon icon-speedometer"></i> Candidates
              </a>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-star"></i> Orders</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="{{route('orders.index')}}">
                    <i class="nav-icon icon-star"></i> All Orders
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('orders.request')}}">
                    <i class="nav-icon icon-star"></i> Customer Requests
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-star"></i> Threads</a>
              <ul class="nav-dropdown-items">
                {{-- <li class="nav-item">
                  <a class="nav-link" href="icons/coreui-icons.html">
                    <i class="nav-icon icon-star"></i> Feedback
                  </a>
                </li> --}}
                <li class="nav-item">
                  <a class="nav-link" href="{{route('complain.index')}}">
                    <i class="nav-icon icon-star"></i> Complain
                  </a>
                </li>
              </ul>
            </li>

            {{-- <li class="nav-title" style="color:#20a8d8">Setting</li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <i class="nav-icon icon-drop"></i> Meta</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <i class="nav-icon icon-pencil"></i> Setting</a>
            </li> --}}

          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>
      <!--sidebar end -->

      <main class="main">

        <!-- Breadcrumb-->
{{-- @include('admin.inc.breadcrumb') --}}


        <div class="container-fluid mt-3">



@yield('content')


        </div>
      </main>
    </div>
{{--
    <footer class="app-footer">
      <div>
        <a href="http://infixsoft.com/">InFixSoft</a>
        <span>&copy; 2018 InFixSoft BD LTD.</span>
      </div>
      <div class="ml-auto">
        <span>Powered by</span>
        <a href="http://infixsoft.com">InFixSoft</a>
      </div>
    </footer> --}}

    <!-- CoreUI and necessary plugins-->
    <script src="{{url('/')}}/coreui/vendors/jquery/js/jquery.min.js"></script>
    <script src="{{url('/')}}/coreui/vendors/popper.js/js/popper.min.js"></script>
    <script src="{{url('/')}}/coreui/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/coreui/vendors/pace-progress/js/pace.min.js"></script>
    <script src="{{url('/')}}/coreui/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
    <script src="{{url('/')}}/coreui/vendors/@coreui/coreui/js/coreui.min.js"></script>


    <!-- for DataTables -->
    <script src="{{url('/')}}/coreui/vendors/datatable/datatables.min.js"></script>
    <script src="{{url('/')}}/coreui/vendors/datatable/dataTables.checkboxes.min.js"></script>

    @stack('scripts')

    <script type="text/javascript">
        $(function(){
          $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
  </body>
</html>
