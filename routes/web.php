<?php
use App\Sessions\Online;

Route::get('/', function () {

    $c = \App\Candidate::find(6);
    return $c->load('review');
    // return view('welcome');
    // return \App\User::all();
    // return unserialize(base64_decode(\App\Sessions\Online::first()->payload));

    // $model = Online::all();
    // $model->map(function ($item, $key) {
    //   $item->payload = collect(
    //       unserialize(base64_decode($item->payload))
    //     );
    //
    //   $item->last_activity = \Carbon\Carbon::createFromTimestamp($item->last_activity)->toDateTimeString();
    // });
    // return $model->all();

    return redirect()->route('dashboard');
});


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function()
{

  Route::get('/', 'DashboardController@index')->name('dashboard');


  Route::get('orders/request', 'OrderRequestController@index')->name('orders.request');
  Route::delete('orders/request/{id}/delete', 'OrderRequestController@destroy')->name('orders.request.destroy');
  Route::post('orders/assign', 'OrderRequestController@assign')->name('orders.assign');
  Route::get('orders/candidate/find', 'OrderRequestController@search')->name('orders.candidate.find');

  Route::resources([
    'services' => 'ServiceController',
    'candidates' => 'CandidateController',
    'orders' => 'OrderController',
    'customers' => 'CustomerController',
  ]);
  Route::put('/candidates/{candidate}/hide', 'CandidateController@hide')->name('candidates.hide');


  Route::get('/feedback', 'FeedbackController@index')->name('feedback.index');
  // Route::get('/feedback/{feedback}/hide', 'FeedbackController@hide')->name('feedback.hide');

  Route::get('/complain', 'ComplainController@index')->name('complain.index');
  // Route::put('/complain/{complain}/hide', 'ComplainController@hide')->name('complain.hide');
  //
  // Route::put('/feedback/{feedback}/hide', 'CandidateController@hide')->name('feedback.hide');
  // Route::put('/feedback/{feedback}/hide', 'CandidateController@hide')->name('feedback.hide');
  //
  // Route::put('/complain/{complain}/hide', 'CandidateController@hide')->name('complain.hide');
  // Route::put('/complain/{complain}/hide', 'CandidateController@hide')->name('complain.hide');

  Route::get('/admin/log/{user}', 'DashboardController@userLog')->name('log');





  Route::get('/chat', function ()
  {
    return view('admin.chat');
  })->name('chat');
  Route::get('/table', function ()
  {
    return view('admin.table');
  })->name('table');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
